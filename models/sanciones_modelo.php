<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sanciones_modelo extends CI_Model{

    public function catSanciones(){
		$this->db->select('id_sancion,nombre');
		$this->db->from('cat_sanciones');
		$this->db->order_by('nombre', 'ASC');
		return $this->db->get()->result();
	}
  public function extrasSancion($id_sancion=NULL){
    $this->db->select('tipo_dato_extra, nombre_extra');
    $this->db->from('cat_sanciones');
    $this->db->where('id_sancion',$id_sancion);
    return $this->db->get()->row();
  }
  public function valoresExtrasSancion($id_sancion=NULL){
    $this->db->select('id_sancion_extra, descripcion');
    $this->db->from('cat_sanciones_extras');
    $this->db->where('id_sancion',$id_sancion);
    return $this->db->get()->result();
  }
}

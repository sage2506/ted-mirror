<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_proceso extends CI_Model{

	public function filas_procesos($id_modulo){
        $this->db->select('procesos.id_proceso');
        $this->db->from("procesos");
        $this->db->join("modulos", "modulos.id_modulo = procesos.id_modulo", "inner");
        $this->db->where('procesos.id_modulo', $id_modulo);
        return $this->db->get()->num_rows();
    }
    public function listado_paginado_procesos($id_modulo,$numeroFilas,$segmento){	
		$this->db->select("procesos.id_proceso, procesos.proceso, procesos.nombre_proceso, modulos.id_modulo, modulos.modulo");
		$this->db->join("modulos", "modulos.id_modulo = procesos.id_modulo", "inner");
		$this->db->where('procesos.id_modulo', $id_modulo);
		$query = $this->db->get('procesos',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();	
	}

	public function filas_procesos_busqueda($referencia,$id_modulo){
        $referencia = $this->db->escape_str($referencia);
		$this->db->select("procesos.id_proceso, procesos.proceso, procesos.nombre_proceso, modulos.id_modulo, modulos.modulo");
		$this->db->from("procesos");
		$this->db->join("modulos", "modulos.id_modulo = procesos.id_modulo", "inner");
		$this->db->where('procesos.id_modulo', $id_modulo);
		$this->db->where("( procesos.proceso LIKE '%$referencia%' OR procesos.nombre_proceso LIKE '%$referencia%' OR modulos.modulo LIKE '%$referencia%' )");
        return $this->db->get()->num_rows();
    }
    public function busqueda_procesos($referencia,$id_modulo,$numeroFilas,$segmento){
		$referencia = $this->db->escape_str($referencia);
		$this->db->select("procesos.id_proceso, procesos.proceso, procesos.nombre_proceso, modulos.id_modulo, modulos.modulo");
		$this->db->join("modulos", "modulos.id_modulo = procesos.id_modulo", "inner");
		$this->db->where('procesos.id_modulo', $id_modulo);
		$this->db->where("( procesos.proceso LIKE '%$referencia%' OR procesos.nombre_proceso LIKE '%$referencia%' OR modulos.modulo LIKE '%$referencia%' )");
		$query = $this->db->get('procesos',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
    }

	public function procesos($id_modulo/*,$numeroFilas,$segment*/){	
		$this->db->select("procesos.id_proceso, procesos.proceso, procesos.nombre_proceso, modulos.id_modulo, modulos.modulo");
		$this->db->join("modulos", "modulos.id_modulo = procesos.id_modulo", "inner");
		$this->db->where('procesos.id_modulo',$id_modulo);
		/*$query = $this->db->get('procesos',$numeroFilas,(($segment>0) ? $segment:0));
		return $query->result();*/
		$this->db->from("procesos");
		return $this->db->get()->result();
	}

	public function guardar(){
		$data['id_modulo'] = $this->input->post('id_modulo');
		$data['proceso'] = $this->input->post('proceso');
		$data['nombre_proceso'] = $this->input->post('nombre_proceso');
		switch ($this->input->post('modo')){
			case 0:return $this->db->insert("procesos", $data); break;
			case 1:
					$this->db->limit(1);
					$this->db->where('id_proceso', $this->input->post('id_proceso'));
					return $this->db->update("procesos", $data);			
				break;
		}	
    }

    public function eliminar($id_proceso = NULL){
		$row = $this->db->get_where("procesos", array("id_proceso" => $id_proceso));
		if ($row->result()) {
			$this->db->limit(1);
			$this->db->where("id_proceso", $id_proceso);
			$this->db->delete("procesos");
		    return TRUE;
		}else{
      		return FALSE;
		}		
    }

	/**
	* Obtiene el proceso.
	*
	* @access public
	* @return object
	*/
	public function obtener_proceso()
	{
		$this->db->select("id_proceso, proceso, nombre_proceso");
		$this->db->from("procesos");
		$this->db->where('id_proceso', $this->input->post('id_proceso'));
		$this->db->limit(1);
		return $this->db->get()->row();
	}
	/**
	* Obtiene el proceso del catálogo.
	*
	* @access public
	* @return object
	*/
	public function proceso_catalogo($id_catalogo, $proceso)
	{
		$this->db->select("catalogos_procesos.id_proceso");
		$this->db->from("catalogos_procesos");
		$this->db->join("procesos", "procesos.id_proceso = catalogos_procesos.id_proceso","INNER");
		$this->db->where("catalogos_procesos.id_catalogo", $id_catalogo);
		$this->db->where("procesos.proceso", $proceso);
		$this->db->limit(1);
			return $this->db->get()->row();
	}	
}
/* End of file modelo_proceso.php */
/* Location: ./application/models/modelo_proceso.php */
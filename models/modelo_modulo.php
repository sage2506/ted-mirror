<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_modulo extends CI_Model{

	public function filas_modulos(){
		$this->db->select('id_modulo');
		$this->db->from("modulos");
		return $this->db->get()->num_rows();	
	}
	public function listado_modulos($numeroFilas, $segmento){
		$this->db->select('id_modulo, id_modulo AS idModulo, modulo, (SELECT COUNT(id_proceso) FROM procesos WHERE id_modulo = idModulo)  AS numero_procesos',FALSE);
		$query = $this->db->get('modulos',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();;	
	}

	function totalFilas_modulosBusqueda($referencia){
		$referencia = $this->db->escape_str($referencia);
		$this->db->like('modulo',$referencia); 
		$query = $this->db->get('modulos');
		return $query->num_rows();
	}

	public function guardar(){
		$data['modulo'] = $this->input->post('modulo');
		switch ($this->input->post('modo')){
			case 0: return $this->db->insert("modulos", $data); break;
			case 1:
				$this->db->limit(1);
				$this->db->where('id_modulo', $this->input->post('id_modulo'));
				return $this->db->update("modulos", $data); break;
		}	
    }

    public function eliminar($id_modulo = NULL){
		$row = $this->db->get_where("modulos", array("id_modulo" => $id_modulo));
		if ($row->result()) {
			$this->db->limit(1);
			$this->db->where("id_modulo", $id_modulo);
			$this->db->delete("modulos");
		    return TRUE;
		}else{
      		return FALSE;
		}		
    }
 
    public function filas_modulos_busqueda($referencia){
        $referencia = $this->db->escape_str($referencia);
		$this->db->select('id_modulo');
		$this->db->from("modulos");
		$this->db->like('modulo',$referencia);
        return $this->db->get()->num_rows();
    }

    public function busqueda_modulos($referencia,$numeroFilas,$segmento){
    	$referencia = $this->db->escape_str($referencia);
		$this->db->select('id_modulo, id_modulo AS idModulo, modulo, (SELECT COUNT(id_proceso) FROM procesos WHERE id_modulo = idModulo)  AS numero_procesos',FALSE);
		$this->db->like('modulo',$referencia);
		$query = $this->db->get('modulos',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
    }

	public function obtener_modulo($id_modulo){
		$this->db->select("id_modulo, modulo AS nombre_modulo");
		$this->db->from("modulos");
		$this->db->where('id_modulo', $id_modulo);
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	public function edicion_modulo(){
		$this->db->select("id_modulo, modulo");
		$this->db->from("modulos");
		$this->db->where('id_modulo', $this->input->post('id_modulo'));
		$this->db->limit(1);
		return $this->db->get()->row();
	}

	public function buscar_modulo($id_modulo){
		$this->db->select("*");
		$this->db->from('modulos');
		$this->db->where("id_modulo", $id_modulo);
		$this->db->limit(1);
		return $this->db->get();
	}
}
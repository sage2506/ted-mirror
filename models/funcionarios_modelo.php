<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Funcionarios_modelo extends CI_Model{

	public function __construct(){
        parent::__construct();
		$this->archivos_path = realpath(APPPATH . '../resoluciones/');
		$this->archivos_path_url = base_url().'resoluciones/';
    }

    public function au_funcionarios($funcionario){
		$this->db->select('id_funcionario,nombre,ap_paterno,ap_materno');
		$this->db->from('funcionarios');
		$this->db->where('activo','1');
		$this->db->where('( nombre LIKE "%'.$funcionario.'%" OR ap_paterno LIKE "%'.$funcionario.'%" OR ap_materno LIKE "%'.$funcionario.'%" )');
		$this->db->order_by('nombre,ap_paterno,ap_materno','ASC');
		$this->db->limit(10);
		return $this->db->get()->result();
    }

    public function filas_busqueda_funcionario($referencia){
		$referencia = $this->db->escape_str($referencia);
		$this->db->select('funcionarios.id_funcionario,funcionarios.nombre,funcionarios.ap_paterno,funcionarios.ap_materno,
							(SELECT COUNT(id_sancionado) FROM sancionados WHERE sancionados.id_funcionario = funcionarios.id_funcionario) AS totSanciones',FALSE);
		$this->db->from('funcionarios');
		$this->db->join('sancionados','sancionados.id_funcionario = funcionarios.id_funcionario','INNER');
		$this->db->join('cat_sanciones','cat_sanciones.id_sancion = sancionados.id_sancion ','INNER');
		$this->db->where('activo','1');
		$this->db->where('(funcionarios.nombre LIKE "%'.$referencia.'%" OR
							funcionarios.ap_paterno LIKE "%'.$referencia.'%" OR
							funcionarios.ap_materno LIKE "%'.$referencia.'%" OR
							cat_sanciones.nombre LIKE "%'.$referencia.'%" )');
		$this->db->group_by('id_funcionario');
		return $this->db->get()->num_rows();
    }

		public function NombreArchivo($id_sancionado=NULL)
		{
			$this->db->select('archivo');
			$this->db->from('sancionados');
			$this->db->where('id_sancionado',$id_sancionado);
			$result = $this->db->get()->row();
			return $result->archivo;

		}

    public function busqueda_funcionario($referencia,$numeroFilas,$segmento){
		$referencia = $this->db->escape_str($referencia);
		$this->db->select('funcionarios.id_funcionario,funcionarios.nombre,funcionarios.ap_paterno,funcionarios.ap_materno,
							(SELECT COUNT(id_sancionado) FROM sancionados WHERE sancionados.id_funcionario = funcionarios.id_funcionario) AS totSanciones',FALSE);
		$this->db->join('sancionados','sancionados.id_funcionario = funcionarios.id_funcionario','INNER');
		$this->db->join('cat_sanciones','cat_sanciones.id_sancion = sancionados.id_sancion ','INNER');
		$this->db->where('activo','1');
		$this->db->where('(funcionarios.nombre LIKE "%'.$referencia.'%" OR
							funcionarios.ap_paterno LIKE "%'.$referencia.'%" OR
							funcionarios.ap_materno LIKE "%'.$referencia.'%" OR
							cat_sanciones.nombre LIKE "%'.$referencia.'%" )');
		$this->db->group_by('id_funcionario');
		$this->db->order_by('nombre,ap_paterno,ap_materno','ASC');
		$query = $this->db->get('funcionarios',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
    }

    public function filasSancionados(){
    	$this->db->select('funcionarios.id_funcionario');
		$this->db->from("funcionarios");
		$this->db->join('sancionados','sancionados.id_funcionario = funcionarios.id_funcionario','INNER');
		$this->db->join('cat_sanciones','cat_sanciones.id_sancion = sancionados.id_sancion ','INNER');
		$this->db->where('activo','1');
		$this->db->group_by('id_funcionario');
		return $this->db->get()->num_rows();
    }

    public function listadoSancionados($numeroFilas,$segmento){
    	$this->db->select('funcionarios.id_funcionario,funcionarios.nombre,funcionarios.ap_paterno,funcionarios.ap_materno,
							(SELECT COUNT(id_sancionado) FROM sancionados WHERE sancionados.id_funcionario = funcionarios.id_funcionario AND estado = "ACTIVO") AS totSanciones',FALSE);
		$this->db->join('sancionados','sancionados.id_funcionario = funcionarios.id_funcionario','INNER');
		$this->db->join('cat_sanciones','cat_sanciones.id_sancion = sancionados.id_sancion ','INNER');
		$this->db->where('activo','1');
		$this->db->group_by('id_funcionario');
		$this->db->order_by('nombre,ap_paterno,ap_materno','ASC');
		$query = $this->db->get('funcionarios',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
    }

    public function seleccion($id_funcionario){
		$this->db->select("id_funcionario,nombre,ap_paterno,ap_materno,direccion,colonia,cp,telefonos,correo");
		$this->db->from("funcionarios");
		$this->db->where("funcionarios.id_funcionario",$id_funcionario);
		return  $this->db->get()->row();
	}

	public function seleccionSancionar($id_funcionario){
		$this->db->select("id_funcionario,nombre,ap_paterno,ap_materno,direccion,colonia,cp,telefonos,correo");
		$this->db->from("funcionarios");
		$this->db->where("funcionarios.id_funcionario",$id_funcionario);
		$this->db->limit(1);
		return  $this->db->get()->result();
	}

	public function ultimosDatos($id_funcionario){
		$this->db->select('cargo,sancionados.id_dependencia,
			(SELECT dependencia FROM dependencias WHERE id_dependencia = sancionados.id_dependencia) AS nombredependencia',FALSE);
		$this->db->from('sancionados');
		$this->db->where('id_funcionario', $id_funcionario);
		$this->db->order_by('id_sancionado', 'DESC');
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	public function verificarFuncionario($id){
		$this->db->select('id_funcionario');
		$this->db->from("funcionarios");
		$this->db->where('id_funcionario',$id);
		$this->db->where('activo','1');
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() == 1) {
	    	return TRUE;
		}else{
	    	return FALSE;
		}
    }

	public function sancionarFuncionario($archivo){
		$dato = array(
			'nombre' => mb_strtoupper($this->input->post('nombre')),
			'ap_paterno' => mb_strtoupper($this->input->post('pat')),
			'ap_materno' => mb_strtoupper($this->input->post('mat')),
			'direccion' => mb_strtoupper($this->input->post('direccion')),
			'colonia' => mb_strtoupper($this->input->post('col')),
			'cp' => mb_strtoupper($this->input->post('cp')),
			'telefonos' => mb_strtoupper($this->input->post('tel')),
			'correo' => mb_strtoupper($this->input->post('correo')),
			'activo' => '1');
			// Si el funcionario no existe aqui lo crea
		if ($this->input->post('id_funcionario') == NULL) {
			$this->db->insert('funcionarios', $dato);
			$funcionario = $this->db->insert_id('funcionarios');
		}else{

			$funcionario = $this->input->post('id_funcionario');
			$this->db->where('id_funcionario',$this->input->post('id_funcionario'));
			$this->db->update('funcionarios', $dato);
		}
		$data = array(
			'id_funcionario	' => $funcionario,
			'cargo' => mb_strtoupper($this->input->post('cargo')),
			'id_dependencia' => $this->input->post('id_dependencia'),
			'id_sancion' => $this->input->post('sancion'),
			'expediente' => mb_strtoupper($this->input->post('numero-expediente')),
			'extracto' => mb_strtoupper($this->input->post('extracto')),
			'archivo' => $archivo);
			// aqui se guarda la sancion
		$this->db->insert('sancionados', $data);
		$sancion_id =$this->db->insert_id();

		$this->db->select('tipo_dato_extra, nombre_extra');
    $this->db->from('cat_sanciones');
    $this->db->where('id_sancion',$this->input->post('sancion'));
    $tipoDatoExtra = $this->db->get()->row();
		$datoExtra = $tipoDatoExtra->tipo_dato_extra;

		if($datoExtra != null){
			$data = array('id_sancionado' => $sancion_id ,
		 								'valor' => $this->input->post('sancionExtra'),
										'tipo' => $datoExtra);
			if($this->db->insert('cat_sanciones_extras_captura',$data)){
				return $funcionario;
			}else {
		 		echo $tipoDatoExtra->tipo_dato_extra;
			}
		}
		return $funcionario;
	}

	public function verificaExtras($id_sancion=NULL){
	  $this->db->select('tipo_dato_extra, nombre_extra');
	  $this->db->from('cat_sanciones');
	  $this->db->where('id_sancion',$id_sancion);
	  $tipoDatoExtra = $this->db->get()->row();
	  return $tipoDatoExtra;
	}

	public function ObtenerExtraDeSancion($id_sancionado=NULL){
	    $this->db->select('valor, tipo');
	    $this->db->from('cat_sanciones_extras_captura');
	    $this->db->where('id_sancionado',$id_sancionado);
	    $resultado = $this->db->get()->row();
	    if($resultado->tipo == 'INPUTS'){
	      return $resultado->valor;
	    }else {
	      $this->db->select('descripcion');
	      $this->db->from('cat_sanciones_extras');
	      $this->db->where('id_sancion_extra', $resultado->valor);
	      return $this->db->get()->row()->descripcion;
	    }
	  }
	public function ejecutarSancion($id_sancionado=NULL,$archivo=NULL)
	{
		$dato = array('archivo_ejecucion' =>  $archivo);
		$this->db->where('id_sancionado', $id_sancionado);
		if($this->db->update('sancionados',$dato)){
			return true;
		}else {
			return fale;
		}
	}

	public function guardar_cambios_funcionario(){
		$dato = array(
					'nombre' => mb_strtoupper($this->input->post('nombre_edicion')),
					'ap_paterno' => mb_strtoupper($this->input->post('pat_edicion')),
					'ap_materno' => mb_strtoupper($this->input->post('mat_edicion')),
					'direccion' => mb_strtoupper($this->input->post('direccion_edicion')),
					'colonia' => mb_strtoupper($this->input->post('col_edicion')),
					'cp' => mb_strtoupper($this->input->post('cp_edicion')),
					'telefonos' => mb_strtoupper($this->input->post('tel_edicion')),
					'correo' => mb_strtoupper($this->input->post('correo_edicion'))
				);
		$this->db->where('id_funcionario',$this->input->post('id_funcionario_edicion'));
		return $this->db->update('funcionarios', $dato);
	}

	public function numSanciones($id_funcionario){
		$this->db->select('id_sancionado');
		$this->db->from('sancionados');
		$this->db->join('funcionarios','funcionarios.id_funcionario = sancionados.id_funcionario','inner');
		$this->db->where('sancionados.id_funcionario',$id_funcionario);
		$this->db->where('funcionarios.activo','1');
		return $this->db->count_all_results();
	}

	public function historialSanciones($id_funcionario){
		$this->db->select('id_sancionado,sancionados.id_sancion as id_sancion, cargo,sancionados.id_dependencia,expediente,cat_sanciones.nombre,extracto,archivo,archivo_ejecucion,
			(SELECT dependencia FROM dependencias WHERE id_dependencia = sancionados.id_dependencia) AS nombre_dependencia',FALSE);
		$this->db->from('sancionados');
		$this->db->join('cat_sanciones','cat_sanciones.id_sancion = sancionados.id_sancion','inner');
		$this->db->join('funcionarios','funcionarios.id_funcionario = sancionados.id_funcionario','inner');
		$this->db->where('sancionados.id_funcionario',$id_funcionario);
		$this->db->where('sancionados.estado', 'ACTIVO');
		$this->db->order_by('id_sancionado','DESC');
		return  $this->db->get()->result();
	}

	public function eliminar_sancion(){
		$eliminar['estado'] = 'INACTIVO';
		$this->db->where('id_sancionado', $this->input->post('id_sancionado'));
		$this->db->limit(1);
		return $this->db->update('sancionados', $eliminar);
	}

	public function crear_constancia(){
		if ( $this->input->post('identificacion') != 'OTRA' ) {
			$identificacion = mb_strtoupper($this->input->post('identificacion'));
		}else{
			$identificacion = mb_strtoupper($this->input->post('nva-identificacion'));
		}
		$info = array(
			'nombre' => mb_strtoupper($this->input->post('nombre')),
			'paterno' => mb_strtoupper($this->input->post('paterno')),
			'materno' => mb_strtoupper($this->input->post('materno')),
			'identificacion' => $identificacion,
			'numero' => mb_strtoupper($this->input->post('numero')),
			'fecha' => date("Y-m-d"));
		$this->db->insert('constancias', $info);
		return $this->db->insert_id('constancias');
	}

	public function informacion_funcionario($id){
		$this->db->select("id_constancia, nombre, paterno, materno, identificacion, numero");
		$this->db->from("constancias");
		$this->db->where("id_constancia",$id);
		return  $this->db->get()->row();
	}

	public function funcionario_sancionado($nombre = NULL, $paterno = NULL, $materno = NULL){
		$this->db->select('cat_sanciones.nombre');
		$this->db->from('sancionados');
		$this->db->join('funcionarios', 'funcionarios.id_funcionario = sancionados.id_funcionario', 'INNER');
		$this->db->join('cat_sanciones', 'cat_sanciones.id_sancion = sancionados.id_sancion', 'INNER');
		$this->db->where('funcionarios.activo', '1');
		$this->db->where('funcionarios.nombre', $nombre);
		$this->db->where('funcionarios.ap_paterno', $paterno);
		$this->db->where('funcionarios.ap_materno', $materno);
		$this->db->order_by('sancionados.id_sancionado', 'DESC');
		$this->db->limit(1);
		return $this->db->get()->row()->nombre;
	}

	public function listado_constancias(){
		$this->db->select('id_constancia, nombre, paterno, materno, identificacion, numero, DATE_FORMAT(fecha, "%d-%m-%Y") AS fecha',FALSE);
		$this->db->from('constancias');
		$this->db->order_by('fecha','DESC');
		$this->db->order_by('paterno, materno, nombre','ASC');
        return $this->db->get()->result();
	}

	public function verificar_sancion($nombre = NULL, $paterno = NULL, $materno = NULL){
		$this->db->select('sancionados.id_sancionado');
		$this->db->from('sancionados');
		$this->db->join('funcionarios', 'funcionarios.id_funcionario = sancionados.id_funcionario', 'INNER');
		$this->db->where('funcionarios.activo', '1');
		$this->db->where('funcionarios.nombre', $nombre);
		$this->db->where('funcionarios.ap_paterno', $paterno);
		$this->db->where('funcionarios.ap_materno', $materno);
		if ( $this->db->get()->num_rows() > 0 ) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

}

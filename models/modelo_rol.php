<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_rol extends CI_Model{

	public function filas_roles(){
        $this->db->select('id_rol');
        $this->db->from("roles");
        return $this->db->get()->num_rows();
    }

   	public function listado_paginado($numeroFilas,$segmento){	
		$this->db->select('id_rol, rol, descripcion, estado');
		$query = $this->db->get('roles',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
	}

   	public function listado(){	
		$this->db->select('id_rol,rol,descripcion,estado');
		$this->db->from("roles");
		$this->db->order_by("id_rol","ASC");
		return $this->db->get()->result();	
	}

	public function select(){
		$this->db->select('id_rol, rol');
		$this->db->from('roles');
		return $this->db->get()->result();
	}

	public function guardar(){
		$data['rol'] = $this->input->post('rol');
		$data['descripcion'] = $this->input->post('descripcion');
		switch ($this->input->post('modo')){
			case 0: return $this->db->insert("roles", $data); break;
			case 1:
					$this->db->limit(1);
					$this->db->where('id_rol', $this->input->post('id_rol'));
					return $this->db->update("roles", $data);			
				break;
		}	
    }

    public function eliminar($id_rol = NULL){
		$row = $this->db->get_where("roles", array("id_rol" => $id_rol));
		if ($row->result()) {
			$this->db->limit(1);
			$this->db->where("id_rol", $id_rol);
			$this->db->delete("roles");
		    return TRUE;
		}else{
      		return FALSE;
		}		
    }
  
    public function filas_roles_busqueda($referencia){
        $referencia = $this->db->escape_str($referencia);
		$this->db->select('id_rol');
		$this->db->from("roles");
		$this->db->like('rol',$referencia);
        return $this->db->get()->num_rows();
    }

    public function busqueda_roles($referencia,$numeroFilas,$segmento){
		$referencia = $this->db->escape_str($referencia);
		$this->db->select('id_rol, rol, descripcion, estado');
		$this->db->like('rol',$referencia);
		$query = $this->db->get('roles',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
    }

	public function obtener_rol($id_rol = NULL){
		if($id_rol != NULL){
			$this->db->select("id_rol, rol AS nombre_rol");
			$this->db->from("roles");
			$this->db->where('id_rol', $id_rol);
			$this->db->limit(1);
			return $this->db->get()->result();
		}else{
			$this->db->select("id_rol, rol, descripcion");
			$this->db->from("roles");
			$this->db->where('id_rol', $this->input->post('id_rol'));
			$this->db->limit(1);
			return $this->db->get()->row();
		}	
	}

	public function obtener_nombre_rol($id_rol = NULL){
		$this->db->select("rol AS nombre_rol");
		$this->db->from("roles");
		$this->db->where('id_rol', $id_rol);
		$this->db->limit(1);
		return $this->db->get()->row();
	}

    public function cambiar_estado($bandera = NULL){	
		$data = array();
		if($this->input->post('estado') == "INACTIVO")
			$data['estado'] = "activo";
		else
			$data['estado'] = "inactivo";
		$this->db->limit(1);
		return $this->db->update("roles", $data ,array('id_rol' => $this->input->post('id_rol')));
    }

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_permiso extends CI_Model{

	function permisos($id_rol){		
		$modulos = array();
		$query = $this->db->query("
			SELECT modulos.id_modulo, modulos.modulo, procesos.id_proceso, procesos.nombre_proceso,
(SELECT COUNT(*) FROM permisos WHERE permisos.id_rol = ".$id_rol." AND permisos.id_modulo = modulos.id_modulo AND permisos.id_proceso = procesos.id_proceso) AS permiso
			FROM modulos
			INNER JOIN procesos ON procesos.id_modulo = modulos.id_modulo
			WHERE modulos.id_modulo
			ORDER BY modulos.id_modulo, modulos.modulo, procesos.id_proceso
		");		
		foreach ($query->result() as $row){
			$modulos[$row->id_modulo][$row->modulo][$row->id_proceso][$row->nombre_proceso] = $row->permiso;
		}
		return $modulos;
	}

	function catalogos_permisos($id_rol){		
		$modulos = array();
		$query = $this->db->query("
			SELECT 
				procesos.id_modulo,
				catalogos.id_catalogo, 
				catalogos.catalogo, 
				catalogos_procesos.id_proceso, 
				procesos.proceso,
               (SELECT COUNT(*) FROM permisos WHERE permisos.id_rol = ".$id_rol." AND permisos.id_modulo = 7 AND permisos.id_proceso = procesos.id_proceso) AS permiso
			FROM catalogos
			INNER JOIN catalogos_procesos ON catalogos_procesos.id_catalogo = catalogos.id_catalogo
			INNER JOIN procesos ON procesos.id_proceso = catalogos_procesos.id_proceso 
			ORDER BY catalogos.catalogo, catalogos_procesos.id_proceso
		");		
		foreach ($query->result() as $row){
			$modulos[7][$row->id_catalogo][$row->catalogo][$row->id_proceso][$row->proceso][$row->id_proceso] = $row->permiso;
		}
		return $modulos;
	}

	public function buscar_permiso(){
		$this->db->select("id_rol, id_modulo, id_proceso");
		$this->db->from('permisos');
		$this->db->where("id_rol", $this->input->post('id_rol'));
		$this->db->where("id_modulo", $this->input->post('id_modulo'));
		$this->db->where("id_proceso", $this->input->post('id_proceso'));
		$this->db->limit(1);
		return $this->db->get();
	}

    public function cambiar_permiso($bandera = NULL){	
		$data = array();
		if ($bandera === TRUE){
			//cambia el permiso.
			$this->db->limit(1);
			$this->db->where("id_rol", $this->input->post('id_rol'));
			$this->db->where("id_modulo", $this->input->post('id_modulo'));
			$this->db->where("id_proceso", $this->input->post('id_proceso'));
			return $this->db->delete("permisos", $data);	
		}else{
			//crea el permiso.
			$data['id_rol'] = $this->input->post('id_rol');
			$data['id_modulo'] = $this->input->post('id_modulo');
			$data['id_proceso'] = $this->input->post('id_proceso');
			$this->db->limit(1);
			return $this->db->insert("permisos", $data);	
		}	
    }

	public function buscar_rol($id_rol){
		$this->db->select("id_rol");
		$this->db->from('roles');
		$this->db->where('estado', 'activo');
		$this->db->where("id_rol", $id_rol);
		$this->db->limit(1);
		return $this->db->get();
	}
}
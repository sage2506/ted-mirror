<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modelo_dependencia extends CI_Model{

    function autocompletar($dependencia){
        $this->db->select('id_dependencia,dependencia');
        $this->db->from('dependencias');
        $this->db->like('dependencia',$dependencia);
        $this->db->order_by('dependencia','ASC');
        $this->db->limit(10);
        return $this->db->get()->result();
    }

    public function filas_dependencias(){
        $this->db->select('id_dependencia');
        $this->db->from("dependencias");
        return $this->db->get()->num_rows();
    }
    public function listado_dependencias_paginado($numeroFilas,$segmento){
        $this->db->select('id_dependencia,parent_id, dependencia');
        $query = $this->db->get('dependencias',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
    }

    function totalFilas_dependenciasBusqueda($referencia){
        $referencia = $this->db->escape_str($referencia);
        $this->db->like('dependencia',$referencia); 
        $query = $this->db->get('dependencias');
        return $query->num_rows();
    }

    function listado_dependencias($numeroFilas,$segment){
        $this->db->select('id_dependencia,parent_id,dependencia');
        $query = $this->db->get('dependencias',$numeroFilas,(($segment>0) ? $segment:0));
        return $query->result();
    }

    public function filas_dependencias_busqueda($referencia){
        $referencia = $this->db->escape_str($referencia);
        $this->db->select('id_dependencia');
        $this->db->from("dependencias");
        $this->db->like('dependencia',$referencia);
        return $this->db->get()->num_rows();
    }
    
    public function buscador_dependencias($referencia,$numeroFilas,$segmento){
        $referencia = $this->db->escape_str($referencia);
        $this->db->select('id_dependencia, parent_id, dependencia');
        $this->db->like('dependencia',$referencia);
        $query = $this->db->get('dependencias',$numeroFilas,(($segmento > 0) ? $segmento:0));
        return $query->result();
    }
  
    function guardar(){
        $data['dependencia'] = $this->input->post('dependencia');
        if ($this->input->post('parent_id')!='') {
            $data['parent_id'] = $this->input->post('parent_id');
        }else{
            $data['parent_id'] = NULL;
        }               
        return $this->db->insert("dependencias", $data);
    }

    function actualizar(){
        $data['dependencia'] = $this->input->post('dependencia');
        if ($this->input->post('parent_id')!='') {
            $data['parent_id'] = $this->input->post('parent_id');
        }else{
            $data['parent_id'] = NULL;
        }
        $this->db->where('id_dependencia', $this->input->post('id_dependencia_ss'));
        $this->db->limit(1);
        return $this->db->update("dependencias", $data);                
    }

    function ExistenciaDependencia($dependencia){
        $this->db->select('id_dependencia');
        $this->db->from("dependencias");
        $this->db->where('dependencia', $dependencia); 
        $this->db->limit(1); 
        $query = $this->db->get();
        if($query->num_rows() == 1) {
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function obtener_dependencia(){
        $this->db->select('id_dependencia, dependencia, parent_id,(SELECT dependencia FROM dependencias WHERE id_dependencia = "'.$this->input->post('parent_id').'") AS nombre_padre', FALSE);
        $this->db->from('dependencias');
        $this->db->where('id_dependencia',$this->input->post('id_dependencia'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    public function eliminar($id_dependencia = NULL){
        $row = $this->db->get_where("dependencias", array("id_dependencia" => $id_dependencia));
        if ($row->result()) {
            $this->db->limit(1);
            $this->db->where("id_dependencia",$id_dependencia);
            $this->db->delete('dependencias');
            return TRUE;
        }else{
            return FALSE;
        }
    }
	
}
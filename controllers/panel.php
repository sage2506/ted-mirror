<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel extends CI_Controller {
	protected $modulo = 8;
	var $permisos;
	var $totalFilas = 20;
    var $num_links = 2;
	public function __construct(){
		parent::__construct();
		$this->permisos = $this->acl->modulo($this->modulo);
		$this->load->model('funcionarios_modelo','modeloFuncionarios');
		$this->load->model('sanciones_modelo','modeloSanciones');
    	$this->load->library('pagination');
    }

	public function index(){
		if ($this->acl->proceso(34)){
			$this->historial();
		}
	}

	public function au_funcionarios(){
		if ( $this->acl->proceso(47) && $this->input->is_ajax_request() ) {
			$funcionario = $this->input->post('termino');
			$result["funcionarios"] = $this->modeloFuncionarios->au_funcionarios($funcionario);
			echo json_encode($result);
		}
    }

	public function sancionar($id = NULL){
		if ( $this->acl->proceso(48) ) {
			if ($id != NULL) {
				if ($this->modeloFuncionarios->verificarFuncionario($id)==TRUE) {
					$info = $this->modeloFuncionarios->seleccionSancionar($id);
					foreach ($info as $campo) {
						$datos['san_id_funcionario'] = $campo->id_funcionario;
						$datos['san_nombre'] = $campo->nombre;
						$datos['san_ap_paterno'] = $campo->ap_paterno;
						$datos['san_ap_materno'] = $campo->ap_materno;
						$datos['san_direccion'] = $campo->direccion;
						$datos['san_colonia'] = $campo->colonia;
						$datos['san_cp'] = $campo->cp;
						$datos['san_telefonos'] = $campo->telefonos;
						$datos['san_correo'] = $campo->correo;
					}
				}else{
					$this->historial();
					return;
				}
			}else{
				$datos['san_id_funcionario'] = '';
				$datos['san_nombre'] = '';
				$datos['san_ap_paterno'] = '';
				$datos['san_ap_materno'] = '';
				$datos['san_direccion'] = '';
				$datos['san_colonia'] = '';
				$datos['san_cp'] = '';
				$datos['san_telefonos'] = '';
				$datos['san_correo'] = '';
			}
			$datos['catSanciones'] = $this->modeloSanciones->catSanciones();
			$info['plantilla'] = $this->load->view('sancionar',$datos,TRUE);
			$this->load->view('plantilla',$info);
		}
	}


public function sancionExtras(){
	$id_tipo_sancion = $this->input->post('id_tipo_sancion');
  $tipoCampos = $this->modeloSanciones->extrasSancion($id_tipo_sancion);
	$valores = $this->modeloSanciones->valoresExtrasSancion($id_tipo_sancion);
  $result = '';
 if($tipoCampos->tipo_dato_extra == 'SELECT'){
	$result = '<div class="control-group" id="cont'.$tipoCampos->nombre_extra.'" >
		<label class="control-label">'.$tipoCampos->nombre_extra.'</label>
		<div class="controls">
			<select name="sancionExtra" id="sancionExtra" class="span12" >
				<option selected="selected" value="0">Seleccione</option>';
					foreach ($valores as $valor ){
						$result .= '<option value="'.$valor->id_sancion_extra.'">'.$valor->descripcion.'</option>';
					}
  $result .= '</select>
			<span class="help-inline" id="msjSancion"></span>
		</div>
	</div>';
}else if($tipoCampos->tipo_dato_extra != NULL){

	foreach ($valores as $valor) {
		$result.='<div class="control-group" id="cont'.$tipoCampos->nombre_extra.'" >
		<label class="control-label">'.$tipoCampos->nombre_extra.'</label>
		<div class="controls">
			<input type="'.$valor->descripcion.'" class="span12" id="sancionExtra" name="sancionExtra" >
			<div></div>
		</div>
	</div>';
	}
 }

 echo ($result);
// echo json_encode($valores);
}


	public function buscarFuncinarios(){
		if ( $this->input->is_ajax_request() && $this->acl->proceso(49) ) {
			$sancionados = $this->modeloFuncionarios->busqueda_funcionario($this->input->post('busqueda'));
			if (count($sancionados) > 0) {
				$tabla = '';
				$tabla .= '<table class="table table-condensed">';
					$tabla.= '<thead>';
						$tabla.= '<tr>';
							$tabla.= '<th style="width:10px">Dependencia</th>';
							$tabla.= '<th style="width:10px">Nombre</th>';
							$tabla.= '<th style="width:10px">Cargo</th>';
							$tabla.= '<th style="width:10px">Sanciones</th>';
							$tabla.= '<th style="width:10px"></th>';
						$tabla.= '</tr>';
				    $tabla.= '</thead>';
				    $tabla.= '<tbody>';
				    if ($sancionados) {
				    	foreach ($sancionados as $campo) {
					    	$tabla.= '<tr>';
						    	$tabla.= '<td>'.$campo->dependencia.'</td>';
						    	$tabla.= '<td>'.$campo->nombre.' '.$campo->ap_paterno.' '.$campo->ap_materno.'</td>';
						    	$tabla.= '<td>'.$campo->cargo.'</td>';
						    	$numSanciones = $this->modeloFuncionarios->numSanciones($campo->id_funcionario);
						    	$tabla.= '<td>'.$numSanciones.'</td>';
						    	$tabla.= '<td>
						    				<div class="btn-group" style="padding:0px;margin-bottom:0px;background-color:transparent;border:0px;-webkit-box-shadow:inset 0 0px 0px rgba(0, 0, 0, 0.05)">
									    		<a data-info="'.$campo->id_funcionario.'" class="selFunc btn btn-success btn-mini opcion" title="Seleccionar"><i class="icon-ok-sign icon-white"></i></a>
									    	</div>
						    			</td>';
					    	$tabla.= '</tr>';
					    }
				    }else{
				    	$tabla.= '<tr>';
				    		$tabla.= '<td colspan="5" >No se encontraron resultados.</td>';
				    	$tabla.= '</tr>';
				    }
					$tabla .= '</tbody>';
				$tabla .= '</table>';
				/*$tabla .= '<script src="'.base_url().'js/bootstrap-tooltip.js"></script>';
				$tabla .= '<script src="'.base_url().'js/application.js"></script>';*/
				$resultado['sancionados'] = $tabla;
				$resultado['exito'] = TRUE;
				$resultado['mensaje'] = '';
			}else{
				$resultado['sancionados'] = '';
				$resultado['exito'] = FALSE;
				$resultado['mensaje'] = 'No se encontraron resultados.';
			}
			echo json_encode($resultado);
		}
	}

	public function seleccion(){
		if ( $this->acl->proceso(50) && $this->input->is_ajax_request() ) {
			$resultado['persona'] = $this->modeloFuncionarios->seleccion($this->input->post('id_funcionario'));
			echo json_encode($resultado);
		}
	}

	public function guardar_cambios_funcionario(){
		if ( $this->acl->proceso(51) && $this->input->is_ajax_request() ) {
			$this->load->library("form_validation");
			$this->form_validation->set_rules('nombre_edicion','Nombre','trim|xss_clean|required');
			$this->form_validation->set_rules('pat_edicion','Apellido Paterno','trim|xss_clean|required');
			$this->form_validation->set_rules('mat_edicion','Apellido Materno','trim|xss_clean|required');
			$this->form_validation->set_rules('cp_edicion','Codigo Postal','trim|xss_clean|min_length[5]|max_length[5]');
			$this->form_validation->set_rules('correo_edicion','Correo Electronico','trim|xss_clean|valid_email');
			if ($this->form_validation->run() === FALSE){
				$resultado["mensaje"]	= $this->form_validation->_error_array;
				$resultado["exito"]		= FALSE;
			}else{
				if ( $this->modeloFuncionarios->guardar_cambios_funcionario() ) {
					$resultado["mensaje"] 	= 'Funcionario editado correctamente';
					$resultado['exito']		= TRUE;
				}else{
					$resultado["mensaje"]	= 'Ocurrio un problema.';
					$resultado['exito']		= FALSE;
				}
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($resultado));
		}
	}
	public function sancionarFuncionario(){
		if ( $this->acl->proceso(51) && $this->input->is_ajax_request() ) {
			$this->load->library("form_validation");
			$this->form_validation->set_rules('id_dependencia','Dependencia','trim|xss_clean|required');
			$this->form_validation->set_rules('nombre','Nombre','trim|xss_clean|required');
			$this->form_validation->set_rules('pat','Apellido Paterno','trim|xss_clean|required');
			$this->form_validation->set_rules('mat','Apellido Materno','trim|xss_clean|required');
			$this->form_validation->set_rules('cargo','Cargo','trim|xss_clean|required');
			$this->form_validation->set_rules('sancion','Sanción','trim|xss_clean|required');
			$this->form_validation->set_rules('cp','Codigo Postal','trim|xss_clean|min_length[5]|max_length[5]');
			$this->form_validation->set_rules('correo','Correo Electronico','trim|xss_clean|valid_email');
			$this->form_validation->set_rules('numero-expediente','Numero de Expediente','trim|xss_clean|required');
			if ($this->form_validation->run() === FALSE){
				$resultado["mensaje"] = $this->form_validation->_error_array;
				$resultado["exito"] = FALSE;
			}else{
				$archivo = 'exp_'.$this->input->post('numero-expediente')
								.'_'.$this->input->post('nombre')
								.'_'.$this->input->post('pat')
								.'_dep_'.$this->input->post('id_dependencia');
				$archivo = str_replace(" ","_",$archivo);
				$archivo = str_replace("á","a",$archivo);
				$archivo = str_replace("é","e",$archivo);
				$archivo = str_replace("í","i",$archivo);
				$archivo = str_replace("ó","o",$archivo);
				$archivo = str_replace("ú","u",$archivo);
				$archivo = str_replace("Á","a",$archivo);
				$archivo = str_replace("É","e",$archivo);
				$archivo = str_replace("Í","i",$archivo);
				$archivo = str_replace("Ó","o",$archivo);
				$archivo = str_replace("Ú","u",$archivo);
				$archivo = str_replace("Ñ","N",$archivo);
				$archivo = str_replace("ñ","n",$archivo);
				$archivo = str_replace(",","_",$archivo);
				$archivo = str_replace("-","_",$archivo);
				$archivo = str_replace("(","_",$archivo);
				$archivo = str_replace(")","_",$archivo);
				$archivo = str_replace("[","_",$archivo);
				$archivo = str_replace("]","_",$archivo);
				$archivo = str_replace("/","_",$archivo);
				$archivo = str_replace(".","_",$archivo);
				$archivo = strtolower($archivo);
				$archivo .= '.pdf';
				$config["upload_path"] = './resoluciones/';
				$config["file_name"] = $archivo;
				$config["allowed_types"] = 'pdf';
				$config["max_size"] = '10240';
				$config["remove_spaces"] = TRUE;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('image1')) {
					$resultado["msj_validacion"] = 'No se realizo la subida del archivo, solo se permiten PDF no mayores de 10 megas.';
					$resultado['exito'] = FALSE;
					$resultado["validacion"] = TRUE;
				}else{
					$resultado['sancionado'] = $this->modeloFuncionarios->sancionarFuncionario($archivo);
					$resultado['exito'] = TRUE;
				}
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($resultado));
		}
	}



	public function subirEjecucion(){
		if ($this->input->is_ajax_request() ) {
			$this->load->library("form_validation");
			$this->form_validation->set_rules('id_sancionado_ejecutar','Sanción','trim|xss_clean|required');
			if ($this->form_validation->run() === FALSE){
				$resultado["mensaje"] = "error";
				$resultado["exito"] = FALSE;
			}else{
				$id_sancionado = $this->input->post('id_sancionado_ejecutar');

				$archivo = 'ejec_'.$this->modeloFuncionarios->NombreArchivo($id_sancionado);
				$config["upload_path"] = './resoluciones/';
				$config["file_name"] = $archivo;
				$config["allowed_types"] = 'pdf';
				$config["max_size"] = '10240';
				$config["remove_spaces"] = TRUE;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('image1')) {
					$resultado['exito'] = FALSE;
					$resultado["validacion"] = TRUE;
					$resultado['mensaje'] = 'No se ha subido el documento';
					$resultado['log'] = $this->upload->display_errors();
				}else{
					$resultado['mensaje'] = "Documento subido exitosamente";
					$resultado['sancionado'] = $this->modeloFuncionarios->ejecutarSancion($id_sancionado,$archivo);
					$resultado['exito'] = TRUE;
				}
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($resultado));
		}
	}
	public function eliminar_sancion(){
		if ( $this->input->is_ajax_request() ) {
			if ( $this->modeloFuncionarios->eliminar_sancion() ) {
				$resultado['exito'] 	= TRUE;
				$resultado["mensaje"]	= 'Se eliminó correctamente la sanción';
			}else{
				$resultado["exito"] 	= FALSE;
				$resultado["mensaje"]	= 'Ocurrio un problema, no se pudo eliminar la sanción';
			}
			echo json_encode($resultado);
		}
	}






	public function historial($sancionado = NULL){
		if ( $this->acl->proceso(34) ) {
			if ( $this->modeloFuncionarios->verificarFuncionario($sancionado) ) {
				$dato['funcionario_sancionado'] = $sancionado;
			}else{
				$dato['funcionario_sancionado'] = NULL;
			}
			$filas = $this->modeloFuncionarios->filasSancionados();
			$this->_paginacion($filas,'panel','paginadoHistorial');
			$resultado['sancionados'] = $this->modeloFuncionarios->listadoSancionados($this->totalFilas,0);
			$dato['listado'] = $this->load->view('historial/resultados',$resultado,TRUE);
			$info['plantilla'] = $this->load->view('historial/historial',$dato,TRUE);
			$this->load->view('plantilla',$info);
		}
	}

	public function paginadoHistorial(){
		if ( $this->acl->proceso(34) && $this->input->is_ajax_request() ) {
			if ( $this->input->post('busqueda') ) {
				$filas = $this->modeloFuncionarios->filas_busqueda_funcionario($this->input->post('busqueda'));
				$this->_paginacion($filas,'panel','paginadoHistorial');
				$resultado['sancionados'] = $this->modeloFuncionarios->busqueda_funcionario($this->input->post('busqueda'),$this->totalFilas,$this->uri->segment(3));
			}else{
				$filas = $this->modeloFuncionarios->filasSancionados();
				$this->_paginacion($filas,'panel','paginadoHistorial');
				$resultado['sancionados'] = $this->modeloFuncionarios->listadoSancionados($this->totalFilas,$this->uri->segment(3));
			}
			$this->load->view('historial/resultados',$resultado);
		}
	}

	public function busquedas(){
		if ( $this->acl->proceso(53) && $this->input->is_ajax_request() ) {
			$filas = $this->modeloFuncionarios->filas_busqueda_funcionario($this->input->post('busqueda'));
			$this->_paginacion($filas,'panel','paginadoHistorial');
			$resultado['sancionados'] = $this->modeloFuncionarios->busqueda_funcionario($this->input->post('busqueda'),$this->totalFilas,$this->uri->segment(3));
			$this->load->view('historial/resultados',$resultado);
		}
	}

	public function historialFuncionario(){
		if ( $this->acl->proceso(54) && $this->input->is_ajax_request() ) {
			$id_funcionario = $this->input->post('id_funcionario');
			$resultado['dependenciaUltima'] = '';
			$resultado['cargoUltimo'] = '';
			$resultado['infoDetalle'] = $this->modeloFuncionarios->seleccionSancionar($id_funcionario);
			$lista = $this->modeloFuncionarios->historialSanciones($id_funcionario);
			$tabla = '<style type="text/css">
						table.table-nowrap {
						        table-layout:fixed;
						}
						.table-nowrap td.corta {
						        overflow:hidden;
						        white-space:nowrap;
						        text-overflow: ellipsis;
						}
						</style>';
			$tabla .= '<script type="text/javascript">
						$(function() {
							$(".opcion").tooltip();
						});
						</script>';
			$tabla .= '<table class="table table-striped table-hover table-condensed table-nowrap" id="tbl">';
				$tabla.= '<thead>';
				$tabla.= '<tr>';
					$tabla.= '<th width="18%">DEPENDENCIA</th>';
					$tabla.= '<th width="20%">CARGO</th>';
					$tabla.= '<th width="14%">SANCIÓN</th>';
					$tabla.= '<th width="6%">EXTRA</th>';
					$tabla.= '<th width="24%">OBSERVACIÓN</th>';
					$tabla.= '<th width="8%"></th>';
					$tabla.= '</tr>';
			    $tabla.= '</thead>';
			    $tabla.= '<tbody>';
			    if ( $lista ) {
			    	foreach ($lista as $campo) {
				    	$tabla.= '<tr>';
				    		$tabla.= '<td class="corta">'.$campo->nombre_dependencia.'</td>';
				    		$tabla.= '<td class="corta">'.$campo->cargo.'</td>';
					    	$tabla.= '<td>'.$campo->nombre.'<br><b>EXPEDIENTE:</b> '.$campo->expediente.'</td>';

								$extraData = $this->modeloFuncionarios->verificaExtras($campo->id_sancion);								
								if($extraData->nombre_extra == NULL){
									$tabla.= '<td></td>';
								}else {
									$extraValores = $this->modeloFuncionarios->ObtenerExtraDeSancion($campo->id_sancionado);
									$tabla.= '<td><b>'.$extraData->nombre_extra.':</b><br> '.$extraValores.'</td>';
								}

					    	$tabla.= '<td class="corta"><a href="javascript:void(0)" style="width:100%; display:block; text-decoration:none; color:#333;">'.$campo->extracto.'</a></td>';
								if($campo->archivo_ejecucion != null){
									$tabla.= '<td>
												<a class="btn btn-success btn-mini pull-right opcion" title="Ejecucion" target="_black" href="'.base_url().'resoluciones/'.$campo->archivo_ejecucion.'"><i class="icon-download-alt icon-white"></i></a>
												<span class="pull-right">&nbsp;</span>';
								}else {
									$tabla.= '<td>
												<a data-id-sancion="'.$campo->id_sancionado.'" data-id-funcionario="'.$this->input->post('id_funcionario').'" class="btn-agregar-ejecución btn btn-mini btn-danger opcion pull-right" title="Ejecutar"><i class="icon-upload-alt icon-white"></i></a>
												<span class="pull-right">&nbsp;</span>';
								}
								$tabla.='<a data-id-sancion="'.$campo->id_sancionado.'" data-id-funcionario="'.$this->input->post('id_funcionario').'" class="btn-eliminar-sancion btn btn-inverse btn-mini pull-right opcion" title="Eliminar"><i class="icon-trash icon-white"></i></a>
					    				<span class="pull-right">&nbsp;</span>
								    	<a class="btn btn-success btn-mini pull-right opcion" title="Sansion" target="_black" href="'.base_url().'resoluciones/'.$campo->archivo.'"><i class="icon-download-alt icon-white"></i></a>
								    </td>';
				    	$tabla.= '</tr>';
				    }
			    }else{
			    	$tabla .= '<tr><td colspan="5">No se encontraron resultados.</td></tr>';
			    }

				$tabla .= '</tbody>';
			$tabla .= '</table>';
			$resultado['historialSanciones'] = $tabla;
			$ultimosDatos = $this->modeloFuncionarios->ultimosDatos($this->input->post('id_funcionario'));
			foreach ($ultimosDatos as $campo) {
				$resultado['dependenciaUltima'] = $campo->nombredependencia;
				$resultado['cargoUltimo'] = $campo->cargo;
			}
			echo json_encode($resultado);
		}
	}



	public function constancias(){
		$info['plantilla'] = $this->load->view('constancias/constancia',NULL,TRUE);
		$this->load->view('plantilla',$info);
	}

	public function listado_constancias(){
		$parametros['constancias'] = $this->modeloFuncionarios->listado_constancias();
		$this->load->view('constancias/constancias_tabla', $parametros);
	}

	public function registro_constancia(){
		if ( $this->input->is_ajax_request() ) {
			$this->load->library("form_validation");
			$this->form_validation->set_rules('nombre','Nombre','trim|xss_clean|required');
			$this->form_validation->set_rules('paterno','Apellido Paterno','trim|xss_clean|required');
			$this->form_validation->set_rules('materno','Apellido Materno','trim|xss_clean|required');
			$this->form_validation->set_rules('identificacion','Identificación','trim|xss_clean|required');
			$this->form_validation->set_rules('numero','Número','trim|xss_clean|required');
			if ($this->form_validation->run() === FALSE){
				$resultado["mensaje"] = $this->form_validation->_error_array;
				$resultado["exito"] = FALSE;
			}else{
				$resultado['id_constancia'] = $this->modeloFuncionarios->crear_constancia();
				$resultado['exito'] = TRUE;
			}
			echo json_encode($resultado);
		}
	}

	public function crear_constancia($id = 0){
		$mes = '';
		$this->output->set_header("Content-type: application/vnd.ms-word");
		$this->output->set_header("Content-disposition: attachment; filename=\"constancia-no-antecedentes-disciplinarios.doc\"");
		switch (date("m")){
			case '01': $mes = 'enero'; break;
			case '02': $mes = 'febrero'; break;
			case '03': $mes = 'marzo'; break;
			case '04': $mes = 'abril'; break;
			case '05': $mes = 'mayo'; break;
			case '06': $mes = 'junio'; break;
			case '07': $mes = 'julio'; break;
			case '08': $mes = 'agosto'; break;
			case '09': $mes = 'septiembre'; break;
			case '10': $mes = 'octubre'; break;
			case '11': $mes = 'noviembre'; break;
			case '12': $mes = 'diciembre'; break;
		}
		$informacion = $this->modeloFuncionarios->informacion_funcionario($id);
		$funcionario_sancionado = $this->modeloFuncionarios->verificar_sancion($informacion->nombre, $informacion->paterno, $informacion->materno);

		$constancia = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html><head><title>formato de carta de no antecedentes disciplinarios</title>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta http-equiv="Content-Style-Type" content="text/css">
			<style type="text/css"><!--
			body {
			 margin: -200px 1px 5px -30px;
			  background-color: #ffffff;
			}
			/* ========== Text Styles ========== */
			hr { color: #000000}
			body, table, span.rvts0 /* Normal text */
			{
			 font-size: 9pt;
			 font-family: "Arial", "Helvetica", sans-serif;
			 font-style: normal;
			 font-weight: normal;
			 color: #000000;
			 text-decoration: none;
			}
			span.rvts1 /* Heading */
			{
			 font-weight: bold;
			 color: #0000ff;
			}
			span.rvts2 /* Subheading */
			{
			 font-weight: bold;
			 color: #000080;
			}
			span.rvts3 /* Keywords */
			{
			 font-style: italic;
			 color: #800000;
			}
			a.rvts4, span.rvts4 /* Jump 1 */
			{
			 color: #008000;
			 text-decoration: underline;
			}
			a.rvts5, span.rvts5 /* Jump 2 */
			{
			 color: #008000;
			 text-decoration: underline;
			}
			span.rvts6
			{
			 font-size: 14pt;
			}
			span.rvts7
			{
			 font-size: 12pt;
			}
			span.rvts8
			{
			 font-size: 12pt;
			 font-weight: bold;
			}
			span.rvts9
			{
				margin-right: -100px;
				margin-left: 300px;
			 font-family: "Times New Roman", "Times", serif;
			 font-weight: bold;
			}
			/* ========== Para Styles ========== */
			p,ul,ol /* Paragraph Style */
			{
			 text-align: left;
			 text-indent: 0px;
			 padding: 0px 0px 0px 0px;
			 margin: 0px 0px 0px 0px;
			}
			.rvps1 /* Centered */
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: center;
			}
			.rvps2
			{
				margin-right: -70px;
				margin-left: 90px;
			 text-align: center;
			}
			.rvps3
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: center;
			}
			.rvps4
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: center;
			}
			.rvps5
			{
			 text-align: center;
			}
			.rvps6
			{
			 text-align: center;
			}
			.rvps7
			{
			 text-align: center;
			}
			.rvps8
			{
				margin-right: -50px;
				margin-left: 15px;
			 text-align: justify;
			}
			.rvps9
			{
			 text-align: justify;
			}
			.rvps10
			{
			}
			.rvps11
			{
			 text-align: justify;
			}
			.rvps12
			{
			 text-align: justify;
			}
			.rvps13
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: justify;
			}
			.rvps14
			{
				margin-right: -50px;
				margin-left: 15px;
			 text-align: justify;
			 text-indent: 47px;
			}
			.rvps15
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: justify;
			 text-indent: 47px;
			}
			.rvps16
			{
				margin-right: -50px;
				margin-left: 15px;
			 text-align: justify;
			 text-indent: 47px;
			}
			.rvps17
			{
			 text-align: justify;
			 text-indent: 47px;
			}
			.rvps18
			{
			 text-align: justify;
			 text-indent: 47px;
			}
			.rvps19
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps20
			{
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps21
			{
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps22
			{
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps23
			{
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps24
			{
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps25
			{
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps26
			{
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps27
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps28
			{
				margin-right: -50px;
				margin-left: 130px;
			 text-align: center;
			 text-indent: 47px;
			}
			.rvps29
			{
			 text-align: justify;
			 margin: 0px 38px 0px 47px;
			 margin-right: -40px;
				margin-left: 25px;
			}
			--></style>
			</head>
			<body style="margin: -200px 1px 5px -30px; text-align:12px;">
			<div style="float: left;  margin-left: -10px;">
				<img src="'.base_url().'img/escudo.png" width="68" height="99" style="margin-left: 30px;" >
				<p style="text-align: justify !important; font-size: 10; margin-left: -27px;">H. AYUNTAMIENTO DE CULIACÁN<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SINDICO PROCURADOR</p>
			</div>
			<br>
			<p><span style="font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DIRECCIÓN DE RESPONSABILIDADES ADMINISTRATIVAS DE LOS SERVIDORES PÚBLICOS</span></p>
			<p class=rvps6><span class=rvts7><br></span></p>
			<div style="display:block-inline">
			<table border=1 cellpadding=2 cellspacing=-1 style="float: left; margin-top: 200px; border-width: 0px; border-collapse: collapse;">
					<tr valign=top>
						<td width=65 height=65 valign=top style="border-width : 1px; border-color: #000000; border-style: solid;">
						</td>
						<td width=400 height=65 align=center style="text-align:center; border-width : 1px; border-color: white; border-style: solid;">				<h3 style="margin-top:30px;" align="center">CONSTANCIA DISCIPLINARIA</h3></td>
					</tr>
				</table>
							</div>
			<br>
			<p class=rvps8><span class=rvts8>A QUIEN CORRESPONDA:</span></p>
			<p class=rvps12><span class=rvts7><br></span></p>';
			if ($informacion->identificacion == 'IFE') {
				# code...
				$ife='credencial para votar expedida por el Instituto Nacional Electoral, con folio';
				$constancia .='<p class=rvps14 style="font-size:10px;"><span class=rvts7>La persona cuya fotografía y huella dactilar que aparecen en este documento, solicitó&nbsp; </span><span class=rvts8>CONSTANCIA DISCIPLINARIA</span><span class=rvts7>, a nombre de:  <span class=rvts8>'.$informacion->nombre.' '.$informacion->paterno.' '.$informacion->materno.'</span>, la cual se identificó con '.$ife.' número <span class=rvts8>'.$informacion->numero.'</span>.&nbsp; </span></p>
			<p><span class=rvts7><br></span></p>';
			}
			else{
				$constancia .='<p class=rvps14 style="font-size:10px;""><span class=rvts7>La persona cuya fotografía y huella dactilar que aparecen en este documento, solicitó&nbsp; </span><span class=rvts8>CONSTANCIA DISCIPLINARIA</span><span class=rvts7>, a nombre de:  <span class=rvts8>'.$informacion->nombre.' '.$informacion->paterno.' '.$informacion->materno.'</span>, la cual se identificó con '.$informacion->identificacion.', número <span class=rvts8>'.$informacion->numero.'</span>.&nbsp; </span></p>
			<p><span class=rvts7><br></span></p>';
			};

		if ( $funcionario_sancionado ) {
			$ultima_sancion = $this->modeloFuncionarios->funcionario_sancionado($informacion->nombre, $informacion->paterno, $informacion->materno);
			$constancia .= '<p class=rvps14 style="font-size:10px;">
								<span class=rvts7>Realizada la consulta procedente en el Sistema de Sanciones a Funcionarios, a cargo de la Dirección de Responsabilidades Administrativas de los Servidores Públicos, quien responde al nombre de referencia </span>
								<span class=rvts8>SI</span><span class=rvts7> registra antecedente disciplinario. <!--en los archivos
									de procedimientos administrativos&nbsp;&nbsp; correspondientes a esta dependencia.
									La sanción fue <span class=rvts8>'.$ultima_sancion .'</span>.--></span>
							</p>';
		}else{
			$constancia .= '<p class=rvps14 style="font-size:10px;">
								<span class=rvts7>Realizada la consulta procedente en el Sistema de Sanciones a Funcionarios, a cargo de la Dirección de Responsabilidades Administrativas de los Servidores Públicos, quien responde al nombre de referencia </span>
								<span class=rvts8>NO</span><span class=rvts7> registra antecedente disciplinario.</span>
							</p>';
		}

		$constancia .= '
			<p class=rvps15><span class=rvts7><br></span></p>
			<p class=rvps16 style="font-size:10px;"><span class=rvts7>Se extiende la presente, de conformidad a lo dispuesto por el artículo 34, fracción III del Reglamento de la Administración Publica del Municipio de Culiacán, Sinaloa, y para los fines que convenga al interesado.
</span></p>

			<p class=rvps17><span class=rvts7><br></span></p>
			<p class=rvps18><span class=rvts7><br></span></p>
			<p style="text-align:center"><span class=rvts7>A t e n t a m e n t e</span></p>
			<p style="text-align:center"><span class=rvts7>Culiacán Rosales, Sinaloa a '.date('d').' de '.$mes.' de '.date('Y').'</span></p>
			<br><br><br><br>
			<p align="center" style="font-size:20px; text-align:center"><span class=rvts8><b>&nbsp;&nbsp;&nbsp;&nbsp;Lic. María Esther Bazúa Ramírez&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lic. Sandra Yudith Lara Díaz</b></span></p>

			<p align="center" style="font-size:16px; text-align:center"><span>Directora de responsabilidades &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Síndica procuradora</span></p>

			<p style="font-size:16px;""><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Administrativas de los Servidores &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
			<p style="font-size:16px;""><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Públicos &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>

			<p class=rvps25><span class=rvts7><br></span></p>
			<p class=rvps26><span class=rvts7><br></span></p><br>
			<p align="center" Style="text-align:center"> <span>_____________________________________</span></p>
			<p style="text-align:center; font-size:16px;"> <span>Firma y huella del interesado</span></p>
			<br>
				<p class=rvps29><span class=rvts9>Este documento cuenta con una vigencia de 60 días contados a partir de la fecha de expedición, asimismo no tiene validez oficial si presenta tachaduras, raspaduras o enmendaduras, así como si no lleva las firmas autógrafas y sello de autorización.</span></p>
			</body></html>';

		$this->output->set_output($constancia);
	}
	private function _paginacion($filastotales,$controlador,$funcion){
        $configPaginacion = array(
            'base_url' => base_url().''.$controlador.'/'.$funcion,
            'total_rows' =>  $filastotales,
            'per_page' => $this->totalFilas,
            'num_links' => $this->num_links,
            'first_link' => '&laquo;',
            'next_link' => '&rsaquo;',
            'prev_link' => '&lsaquo;',
            'last_link' => '&raquo;',
            'full_tag_open' => '<div id="paginacion" class="pagination pagination-right"><ul>',
            'full_tag_close' => '</ul></div>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'page_query_string' => FALSE,
            'query_string_segment' => "per_page",
            'display_pages' => TRUE,
            'anchor_class' => 'class="btn-paginar"');
        $this->pagination->initialize($configPaginacion);
    }

}

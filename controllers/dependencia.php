﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dependencia extends CI_Controller {
	protected $modulo = 12;
	var $permisos;
	var $totalFilas = 20;
    var $num_links = 2;
	public function __construct(){
        parent::__construct();
		$this->permisos = $this->acl->modulo($this->modulo);
		$this->load->model('modelo_usuario','usuarioModelo');
		$this->load->model('modelo_dependencia','dependenciaModelo');
		$this->load->library('pagination');
	}

	private function _paginacion($numFilas,$filastotales,$controlador,$funcion){
        $configPaginacion = array(
            'base_url' => base_url().''.$controlador.'/'.$funcion,
            'total_rows' =>  $filastotales,
            'per_page' => $numFilas,
            'num_links' => $this->num_links,
            'first_link' => '&laquo;',
            'next_link' => '&rsaquo;',
            'prev_link' => '&lsaquo;',
            'last_link' => '&raquo;',
            'full_tag_open' => '<div id="paginacion" class="pagination pagination-right"><ul>',
            'full_tag_close' => '</ul></div>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'page_query_string' => FALSE,
            'query_string_segment' => "per_page",
            'display_pages' => TRUE,
            'anchor_class' => 'class="btn-paginar"');
        $this->pagination->initialize($configPaginacion);
    } 

	public function listado(){
		if ($this->acl->proceso(18)) {
			$filas = $this->dependenciaModelo->filas_dependencias();
            $this->_paginacion($this->totalFilas,$filas,'dependencia','paginacionDep');
			$parametros['dependencias'] = $this->dependenciaModelo->listado_dependencias_paginado($this->totalFilas,$this->uri->segment(3));
			$params['plantilla'] = $this->load->view('seguridad/dependencias',$parametros,TRUE);
			$this->load->view('plantilla', $params);
		}		
	}

	public function paginacionDep(){
		if ( $this->input->is_ajax_request()  ) {
			if ( $this->input->post('referencia') ) {
				$filas = $this->dependenciaModelo->filas_dependencias_busqueda($this->input->post('referencia'));
	        	$this->_paginacion($this->totalFilas,$filas,'dependencia','paginacionDep');
				$parametros['dependencias'] = $this->dependenciaModelo->buscador_dependencias($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));
			}else{
				$filas = $this->dependenciaModelo->filas_dependencias();
            	$this->_paginacion($this->totalFilas,$filas,'dependencia','paginacionDep');
				$parametros["dependencias"] = $this->dependenciaModelo->listado_dependencias_paginado($this->totalFilas,$this->uri->segment(3));
			}
			$this->load->view('seguridad/dependencia_tabla', $parametros);
    	}
	}

	public function autocompletar(){
		if ($this->acl->proceso(19)) {
			$dependencia = $this->input->post('termino', TRUE);
			$result["dependencias"] = $this->dependenciaModelo->autocompletar($dependencia);
			echo json_encode($result);
		}		
    }
	
    public function alta(){
    	if ($this->acl->proceso(20)) {
			if($this->input->is_ajax_request()){
				if ($this->input->post('modo')=='1') {
					$this->actualizar();
				}else{
					$this->load->library("form_validation");        
					$this->form_validation->set_rules('dependencia', 'Dependencia','trim|xss_clean|required|callback_existe_dependencia');
					if ($this->form_validation->run() === FALSE){            
						$resultado['mensaje'] = current($this->form_validation->_error_array);
						$resultado['exito'] = FALSE;
					}else{
						$this->dependenciaModelo->guardar();	
						$resultado['mensaje'] = "Dependencia guardada satisfactoriamente";
						$resultado['exito'] = TRUE;
					}   
					echo json_encode($resultado);
				}
			}
		}
    }

    public function existe_dependencia($dependencia){
    	if ($this->acl->proceso(21)) {
			$existe = $this->dependenciaModelo->ExistenciaDependencia($dependencia);
			if ($existe === TRUE){
		    	$this->form_validation->set_message('existe_dependencia', 'La Dependencia "'.$dependencia.'" ya existe, favor de verificar.');
			    return FALSE;
			}else{
		    	return TRUE;
			}
		}
    }

	public function edicion(){
		if ($this->acl->proceso(22)) {
			if($this->input->is_ajax_request()){	
				$this->form_validation->set_rules('id_dependencia', 'id_dependencia', 'trim|xss_clean|required');
				if ($this->form_validation->run()){
					$result['dependencia'] = $this->dependenciaModelo->obtener_dependencia();
					echo json_encode($result);
				}
			}
		}		
	}

	public function actualizar(){
		if ($this->acl->proceso(23)) {
			$this->load->library("form_validation");        
			$this->form_validation->set_rules('dependencia','Dependencia','trim|xss_clean|required');
			if ($this->form_validation->run() === FALSE){            
				$resultado['mensaje'] = current($this->form_validation->_error_array);
				$resultado['exito'] = FALSE;
			}else{
				$this->dependenciaModelo->actualizar();
				$resultado['mensaje'] = "Dependencia editada satisfactoriamente";
				$resultado['exito'] = TRUE;
			}   
			echo json_encode($resultado);
		}		
	}
	
    public function eliminar($id_dependencia = 0){
    	if ($this->acl->proceso(24)) {
			if($this->input->is_ajax_request()){
				if ($this->dependenciaModelo->eliminar($id_dependencia)){			
					$result['msj'] = "Se ha eliminado correctamente.";
					$result['exito'] = TRUE;	
				}else{
					$result['msj'] = "No se pudo eliminar.";
					$result['exito'] = FALSE;	
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
    }

    public function busqueda(){
    	if ($this->acl->proceso(25) && $this->input->is_ajax_request()) {
			$filas = $this->dependenciaModelo->filas_dependencias_busqueda($this->input->post('referencia'));
        	$this->_paginacion($this->totalFilas,$filas,'dependencia','paginacionDep');
			$parametros['dependencias'] = $this->dependenciaModelo->buscador_dependencias($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));
			$this->load->view('seguridad/dependencia_tabla', $parametros);
		}		
	}
    
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modulo extends CI_Controller {
	protected $modulo = 9;
	var $permisos;
	var $totalFilas = 20;
    var $num_links = 2;
	public function __construct() {
		parent::__construct();
		$this->permisos = $this->acl->modulo($this->modulo);
		$this->load->model('modelo_modulo','moduloModelo');
		$this->load->model('modelo_proceso','procesoModelo');
		$this->load->library('pagination');
	}

	private function _paginacion($numFilas,$filastotales,$controlador,$funcion){
        $configPaginacion = array(
            'base_url' => base_url().''.$controlador.'/'.$funcion,
            'total_rows' =>  $filastotales,
            'per_page' => $numFilas,
            'num_links' => $this->num_links,
            'first_link' => '&laquo;',
            'next_link' => '&rsaquo;',
            'prev_link' => '&lsaquo;',
            'last_link' => '&raquo;',
            'full_tag_open' => '<div id="paginacion" class="pagination pagination-right"><ul>',
            'full_tag_close' => '</ul></div>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'page_query_string' => FALSE,
            'query_string_segment' => "per_page",
            'display_pages' => TRUE,
            'anchor_class' => 'class="btn-paginar"');
        $this->pagination->initialize($configPaginacion);
    }

	public function listado() {
		if ($this->acl->proceso(2)) {
			$filas = $this->moduloModelo->filas_modulos();
            $this->_paginacion($this->totalFilas,$filas,'modulo','paginacionMod');
			$parametros['modulos'] = $this->moduloModelo->listado_modulos($this->totalFilas,$this->uri->segment(3));
			$dato['tabla'] = $this->load->view('seguridad/modulos_tabla', $parametros, TRUE);
			$params['plantilla'] = $this->load->view('seguridad/modulos', $dato, TRUE);
			$this->load->view('plantilla',$params);
		}		
	}

	public function paginacionMod(){
		if ( $this->acl->proceso(2) ) {
			if ( $this->input->post('referencia') ) {
				$filas = $this->moduloModelo->filas_modulos_busqueda($this->input->post('referencia'));
		        $this->_paginacion($this->totalFilas,$filas,'modulo','paginacionMod');
				$parametros['modulos'] = $this->moduloModelo->busqueda_modulos($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));
			}else{
				$filas = $this->moduloModelo->filas_modulos();
	            $this->_paginacion($this->totalFilas,$filas,'modulo','paginacionMod');
				$parametros['modulos'] = $this->moduloModelo->listado_modulos($this->totalFilas,$this->uri->segment(3));
			}
			$this->load->view('seguridad/modulos_tabla', $parametros);
		}
	}

	public function alta(){
		if ($this->acl->proceso(3)) {
			if($this->input->is_ajax_request()){
				$this->load->library("form_validation");
				$this->form_validation->set_rules('modulo', 'Módulo', 'trim|xss_clean|required');
				if ($this->form_validation->run() === FALSE){
					$result['msj'] = current($this->form_validation->_error_array);
					$result['exito'] = FALSE;	
				}else{
					$this->moduloModelo->guardar();
					$result['msj'] = "Módulo guardado satisfactoriamente";
					$result['exito'] = TRUE;
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function eliminar($id_modulo = 0){
		if ($this->acl->proceso(4)) {
			if($this->input->is_ajax_request()){
				if ($this->moduloModelo->eliminar($id_modulo)){
					$result['msj'] = "Se ha eliminado correctamente.";
					$result['exito'] = TRUE;	
				}else{
					$result['msj'] = "No se pudo eliminar.";
					$result['exito'] = FALSE;	
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function busqueda(){
		if ($this->acl->proceso(5)) {
			$filas = $this->moduloModelo->filas_modulos_busqueda($this->input->post('referencia'));
	        $this->_paginacion($this->totalFilas,$filas,'modulo','paginacionMod');
			$parametros['modulos'] = $this->moduloModelo->busqueda_modulos($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));
			$this->load->view('seguridad/modulos_tabla', $parametros);
		}		
	}

	public function proceso($id_modulo=NULL){
		if ($this->acl->proceso(6)) {
			$parametros['idModulo'] = $id_modulo;
			$parametros['modulo'] = $this->moduloModelo->obtener_modulo($id_modulo);
			$filas = $this->procesoModelo->filas_procesos($id_modulo);
    		$this->_paginacion($this->totalFilas,$filas,'proceso','paginacionPro');
			$parametros['procesos'] = $this->procesoModelo->listado_paginado_procesos($id_modulo,$this->totalFilas,$this->uri->segment(4));
			$params['plantilla'] = $this->load->view('seguridad/procesos', $parametros, TRUE);
			$this->load->view('plantilla',$params);
		}		
	}	

}
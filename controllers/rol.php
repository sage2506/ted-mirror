<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rol extends CI_Controller {
	protected $modulo = 11;
	var $permisos;
	var $totalFilas = 20;
    var $num_links = 2;
	public function __construct() {
		parent::__construct();
		$this->permisos = $this->acl->modulo($this->modulo);
		$this->load->model('modelo_rol','rolModelo');
		$this->load->model('modelo_permiso','permisoModelo');
		$this->load->library('pagination');		
	}

	public function listado() {
		if ($this->acl->proceso(10)) {
			$filas = $this->rolModelo->filas_roles();
            $this->_paginacion($this->totalFilas,$filas,'rol','paginacionRol');
			$parametros['roles'] = $this->rolModelo->listado_paginado($this->totalFilas,$this->uri->segment(3));
			$params['plantilla'] = $this->load->view('seguridad/roles', $parametros, TRUE);
			$this->load->view('plantilla',$params);
		}
	}

	public function paginacionRol(){
		if ( $this->acl->proceso(10) && $this->input->is_ajax_request() ) {
			if ( $this->input->post('referencia') ) {
				$filas = $this->rolModelo->filas_roles_busqueda($this->input->post('referencia'));
		       	$this->_paginacion($this->totalFilas,$filas,'rol','paginacionRol');
				$parametros['roles'] = $this->rolModelo->busqueda_roles($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));
			}else{
				$filas = $this->rolModelo->filas_roles();
		        $this->_paginacion($this->totalFilas,$filas,'rol','paginacionRol');
				$parametros['roles'] = $this->rolModelo->listado_paginado($this->totalFilas,$this->uri->segment(3));
			}
			$this->load->view('seguridad/roles_tabla', $parametros);
		}
	}

	public function alta(){
		if ($this->acl->proceso(11)) {
			if($this->input->is_ajax_request()){
				$this->load->library("form_validation");	
				$this->form_validation->set_rules('rol', 'Rol', 'trim|xss_clean|required');
				$this->form_validation->set_rules('descripcion', 'Descripción', 'trim|xss_clean|required');
				if ($this->form_validation->run() === FALSE){
					$result['msj'] = $this->form_validation->_error_array;
					$result['exito'] = FALSE;	
				}else{
					$this->rolModelo->guardar();
					$result['msj'] = 'Rol guardado satisfactoriamente';
					$result['exito'] = TRUE;
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function edicion(){
		if ($this->acl->proceso(12)) {
			if($this->input->is_ajax_request()){
				$this->form_validation->set_rules('id_rol', 'id_rol', 'trim|xss_clean|required');
				if ($this->form_validation->run()){
					$result['rol'] = $this->rolModelo->obtener_rol();
					echo json_encode($result);
				}
			}
		}
	}

	public function eliminar($id_rol = 0){
		if ($this->acl->proceso(13)) {
			if($this->input->is_ajax_request()){
				if ($this->rolModelo->eliminar($id_rol)){
					$result['msj'] = "Se ha eliminado correctamente.";
					$result['exito'] = TRUE;
				}else{
					$result['exito'] = FALSE;
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function busqueda(){
		if ($this->acl->proceso(14)) {
			$filas = $this->rolModelo->filas_roles_busqueda($this->input->post('referencia'));
	       	$this->_paginacion($this->totalFilas,$filas,'rol','paginacionRol');
			$parametros['roles'] = $this->rolModelo->busqueda_roles($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));	
			$this->load->view('seguridad/roles_tabla', $parametros);
		}
	}

	public function permisos($id_rol = NULL){
		if ($this->acl->proceso(17)) {
			if ($id_rol == NULL) {
				$this->listado();
			}else{
				/*$rol = $this->permisoModelo->buscar_rol($id_rol);
				if ($rol->num_rows() == 1) {*/
					$data['menu'] = $this->permisoModelo->permisos($id_rol);
					$data['permisos'] = $this->rolModelo->obtener_rol($id_rol);
					$dato = $this->rolModelo->obtener_nombre_rol($id_rol);
					$data['nombre_rol'] = $dato->nombre_rol;
					$data['id_rol'] = $id_rol;
					$params['plantilla'] = $this->load->view('seguridad/permisos',$data,TRUE);
					$this->load->view('plantilla',$params);	
				/*}else{
					$this->listado();    
				}*/
			}
		}		
	}

	public function cambiar_permiso(){
		if ($this->acl->proceso(15)) {
			if($this->input->is_ajax_request()) {
				$bandera = FALSE;
	            $permiso = $this->permisoModelo->buscar_permiso();
	            if ($permiso->num_rows() == 1){
					$bandera = TRUE;
					if ($this->permisoModelo->cambiar_permiso($bandera)){
						$result['msg'] = "editado";
						$result['modo'] = "<span class='badge badge-important'><i class='icon-remove icon-white'></i></span>";
						$result['success'] = TRUE;
					}else{
						$result['msg'] = "No se puede cambiar el permiso";
						$result['success'] = FALSE;	
					}
				}else{  
					$bandera = FALSE;
					if ($this->permisoModelo->cambiar_permiso($bandera)){
						$result['msg'] = "creado";
						$result['modo'] = "<span class='badge badge-success'><i class='icon-ok icon-white'></i></span>";
						$result['success'] = TRUE;
					}else{
						$result['msg'] = "No se puede cambiar el permiso";
						$result['success'] = FALSE;
					}
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}	

	public function cambiar_estado(){
		if ($this->acl->proceso(16)) {
			if($this->input->is_ajax_request()) {
				//cambiamos el estado
				if ($this->rolModelo->cambiar_estado()){
					$result['success'] = TRUE;
					if ( $this->input->post('estado') == "ACTIVO" ) {
						$result['msj'] = "Se DESACTIVO al usuario.";
						$result['estado'] = TRUE;
					}else{
						$result['msj'] = "Se ACTIVO al usuario.";
						$result['estado'] = FALSE;
					}
				}else{
					$result['msg'] = "No se puedo cambiar el estado";
					$result['success'] = FALSE;	
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	private function _paginacion($numFilas,$filastotales,$controlador,$funcion){
        $configPaginacion = array(
            'base_url' => base_url().''.$controlador.'/'.$funcion,
            'total_rows' =>  $filastotales,
            'per_page' => $numFilas,
            'num_links' => $this->num_links,
            'first_link' => '&laquo;',
            'next_link' => '&rsaquo;',
            'prev_link' => '&lsaquo;',
            'last_link' => '&raquo;',
            'full_tag_open' => '<div id="paginacion" class="pagination pagination-right"><ul>',
            'full_tag_close' => '</ul></div>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'page_query_string' => FALSE,
            'query_string_segment' => "per_page",
            'display_pages' => TRUE,
            'anchor_class' => 'class="btn-paginar"');
        $this->pagination->initialize($configPaginacion);
    }
}
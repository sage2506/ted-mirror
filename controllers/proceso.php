<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proceso extends CI_Controller{
	protected $modulo = 10;
	var $permisos;
	var $totalFilas = 20;
    var $num_links = 2;
	public function __construct() {
		parent::__construct();
		$this->permisos = $this->acl->modulo($this->modulo);
		$this->load->model('modelo_proceso','procesoModelo');
		$this->load->library('pagination');
	}

	public function paginacionPro(){
		if ( $this->input->is_ajax_request() ) {
			if ( $this->input->post('referencia') ) {
				$filas = $this->procesoModelo->filas_procesos_busqueda($this->input->post('referencia'),$this->input->post('idModulo'));
		        $this->_paginacion($this->totalFilas,$filas,'proceso','paginacionPro');
				$parametros['procesos'] = $this->procesoModelo->busqueda_procesos($this->input->post('referencia'),$this->input->post('idModulo'),$this->totalFilas,$this->uri->segment(3));
			}else{
				$filas = $this->procesoModelo->filas_procesos($this->input->post('idModulo'));
				$this->_paginacion($this->totalFilas,$filas,'proceso','paginacionPro');
				$parametros['procesos'] = $this->procesoModelo->listado_paginado_procesos($this->input->post('idModulo'),$this->totalFilas,$this->uri->segment(3));
			}			
			$this->load->view('seguridad/procesos_tabla', $parametros);
		}		
	}

	public function busqueda(){
		if( $this->input->is_ajax_request() ) {
			$filas = $this->procesoModelo->filas_procesos_busqueda($this->input->post('referencia'),$this->input->post('idModulo'));
	        $this->_paginacion($this->totalFilas,$filas,'proceso','paginacionPro');
			$parametros['procesos'] = $this->procesoModelo->busqueda_procesos($this->input->post('referencia'),$this->input->post('idModulo'),$this->totalFilas,$this->uri->segment(3));
			$this->load->view('seguridad/procesos_tabla', $parametros);
		}
	}

	public function alta(){
		if ($this->acl->proceso(7)) {
			if($this->input->is_ajax_request()){
				$this->load->library("form_validation");			
				$this->form_validation->set_rules('nombre_proceso', 'Nombre del Proceso', 'trim|xss_clean|required');
				$this->form_validation->set_rules('proceso', 'Proceso', 'trim|xss_clean|required|callback_soloMinusculasGuion');
				if ($this->form_validation->run() === FALSE){
					$result['msj'] = $this->form_validation->_error_array;
					$result['exito'] = FALSE;	
				}else{
					$this->procesoModelo->guardar();
					$result['msj'] = "Proceso guardado satisfactoriamente";
					$result['exito'] = TRUE;
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function edicion(){
		if ($this->acl->proceso(8)) {
			if($this->input->is_ajax_request()){		
				$this->form_validation->set_rules('id_proceso', 'id_proceso', 'trim|xss_clean|required');
				if ($this->form_validation->run()){
					$result['proceso'] = $this->procesoModelo->obtener_proceso();
					echo json_encode($result);
				}
			}
		}		
	}

	public function eliminar($id_proceso = 0){
		if ($this->acl->proceso(9)) {
			if($this->input->is_ajax_request()){
				if ($this->procesoModelo->eliminar($id_proceso)){
					$result['msj'] = "Se ha eliminado correctamente.";
					$result['exito'] = TRUE;	
				}else{
					$result['msj'] = "No se pudo eliminar.";
					$result['exito'] = FALSE;	
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function soloMinusculasGuion($string){
		if (!(preg_match("/^([a-z_])+$/i", $string))){
			$this->form_validation->set_message('soloMinusculasGuion', 'El %s solo debe contener letras y guion bajo.');
            return FALSE;
        }else{
            return TRUE;
        }
	}

	private function _paginacion($numFilas,$filastotales,$controlador,$funcion){
        $configPaginacion = array(
            'base_url' => base_url().''.$controlador.'/'.$funcion,
            'total_rows' =>  $filastotales,
            'per_page' => $numFilas,
            'num_links' => $this->num_links,
            'first_link' => '&laquo;',
            'next_link' => '&rsaquo;',
            'prev_link' => '&lsaquo;',
            'last_link' => '&raquo;',
            'full_tag_open' => '<div id="paginacion" class="pagination pagination-right"><ul>',
            'full_tag_close' => '</ul></div>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'page_query_string' => FALSE,
            'query_string_segment' => "per_page",
            'display_pages' => TRUE,
            'anchor_class' => 'class="btn-paginar"');
        $this->pagination->initialize($configPaginacion);
    }
}
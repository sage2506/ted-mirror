<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller{
	protected $modulo = 13;
	var $permisos;
	var $totalFilas = 20;
    var $num_links = 2;
	var $resultado;
	public function __construct(){
		parent::__construct();
		$this->permisos = $this->acl->modulo($this->modulo);
		$this->load->model('modelo_usuario','usuarioModelo');
		$this->load->model('modelo_rol','rolModelo');
		$this->resultado = new stdClass();
		$this->load->library('pagination');
	}

	private function _paginacion($numFilas,$filastotales,$controlador,$funcion){
        $configPaginacion = array(
            'base_url' => base_url().''.$controlador.'/'.$funcion,
            'total_rows' =>  $filastotales,
            'per_page' => $numFilas,
            'num_links' => $this->num_links,
            'first_link' => '&laquo;',
            'next_link' => '&rsaquo;',
            'prev_link' => '&lsaquo;',
            'last_link' => '&raquo;',
            'full_tag_open' => '<div id="paginacion" class="pagination pagination-right"><ul>',
            'full_tag_close' => '</ul></div>',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'page_query_string' => FALSE,
            'query_string_segment' => "per_page",
            'display_pages' => TRUE,
            'anchor_class' => 'class="btn-paginar"');
        $this->pagination->initialize($configPaginacion);
    } 

	public function index(){
		$this->listado();
	}

	public function listado(){
		if ($this->acl->proceso(26)) {
			$parametros['roles'] = $this->rolModelo->listado();
			$parametros['select'] = $this->rolModelo->select();
			$filas = $this->usuarioModelo->filas_usuarios();
            $this->_paginacion($this->totalFilas,$filas,'usuario','paginacionUsu');
			$parametros['usuarios'] = $this->usuarioModelo->listado_paginado($this->totalFilas,$this->uri->segment(3));
			$params['plantilla'] = $this->load->view('seguridad/usuarios', $parametros, TRUE);
			$this->load->view('plantilla', $params);
		}		
	}

	public function paginacionUsu(){
		if ( $this->input->is_ajax_request()  ) {
			if ( $this->input->post('referencia') ) {
				$filas = $this->usuarioModelo->filas_usuarios_busqueda($this->input->post('referencia'));
	       		$this->_paginacion($this->totalFilas,$filas,'usuario','paginacionUsu');
				$parametros['usuarios'] = $this->usuarioModelo->busqueda_usuario($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));
			}else{
				$filas = $this->usuarioModelo->filas_usuarios();
	            $this->_paginacion($this->totalFilas,$filas,'usuario','paginacionUsu');
				$parametros['usuarios'] = $this->usuarioModelo->listado_paginado($this->totalFilas,$this->uri->segment(3));
			}
			$this->load->view('seguridad/usuarios_tabla', $parametros);
    	}
	}

	public function alta(){
		if ($this->acl->proceso(27)) {
			if($this->input->is_ajax_request()){
				$this->load->library("form_validation");					
				$this->form_validation->set_rules('nombre', 'Nombre', 'trim|xss_clean|required');			
				$this->form_validation->set_rules('id_rol', 'Grupo', 'trim|xss_clean|required');
				$this->form_validation->set_rules('correo','Correo Electr&oacute;nico','trim|xss_clean|required|valid_email');
				if ( $this->input->post('modo') == '0' ) {
					$this->form_validation->set_rules('usuario', 'Usuario', 'trim|xss_clean|required|alpha_numeric|min_length[8]|max_length[10]|callback_existe_usuario');
					$this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|xss_clean|required|min_length[8]|max_length[20]|callback_no_permitir_nombre');
					$this->form_validation->set_rules('confirm_password', 'Repetir Contrase&ntilde;a', 'trim|xss_clean|required|matches[password]');
				}						
				if ($this->form_validation->run() === FALSE){
					$resultado['msj'] = $this->form_validation->_error_array;
					$resultado['exito'] = FALSE;	
				}else{
					$this->usuarioModelo->guardar();
					$result['msj'] = 'Usuario guardado satisfactoriamente.';
					$resultado['exito'] = TRUE;
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($resultado);
			}
		}		
	}

	public function existe_usuario($usuario){
		if ($this->acl->proceso(27)) {
			$existe = $this->usuarioModelo->ExistenciaUsuario($usuario);
			if($existe === TRUE){
				$this->form_validation->set_message('existe_usuario', 'El Usuario "'.$usuario.'" ya existe, elija otro.');
				return FALSE;		
			}else{
			    return TRUE;		
			}
		}			
	}

	public function no_permitir_nombre($password){
		if ($this->acl->proceso(27)) {
			$usuario = $this->input->post('usuario');
			if($usuario !== $password){
				return TRUE;
			}else{
				$this->form_validation->set_message('no_permitir_nombre', 'T&uacute; nombre de usuario y contrase&ntilde;a son iguales. Por razones de seguridad no se permite.');
				return FALSE;
			}
		}		
	}

	public function comprobar_correo(){
		if ($this->acl->proceso(28)) {
			if($this->input->is_ajax_request()){	
				if($this->usuarioModelo->ExistenciaCorreo() === TRUE){
					$result['exito'] = TRUE;
					$result['msj'] = "";
				}else{
					$result['exito'] = FALSE;
					$result['msj'] = "Correo electr&oacute;nico ya est&aacute; en uso.";
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function eliminar($id_usuario = 0){
		if ($this->acl->proceso(29)) {
			if($this->input->is_ajax_request()){
				if ($this->usuarioModelo->eliminar($id_usuario)){
					$resultado['msj'] = "Se ha eliminado correctamente.";
					$resultado['exito'] = TRUE;	
				}else{
					$resultado['msj'] = "No se pudo eliminar.";
					$resultado['exito'] = FALSE;	
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($resultado);
			}
		}		
	}

	public function cambiar_estado(){
		if ($this->acl->proceso(30)) {
			if($this->input->is_ajax_request()) {
				if ($this->usuarioModelo->cambiar_estado()){
					$result['success'] = TRUE;
					if($this->input->post('estado') == "ACTIVO"){
						$result['msj'] = "Se DESACTIVO al usuario.";
						$result['estado'] = TRUE;
					}else{
						$result['msj'] = "Se ACTIVO al usuario.";
						$result['estado'] = FALSE;
					}
				}else{
					$result['msj'] = "No se puedo cambiar el estado";
					$result['success'] = FALSE;	
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}		
	}

	public function cambiarContrasena(){
		if ($this->acl->proceso(31)) {
			if($this->input->is_ajax_request()){		
				$this->load->library('form_validation');
				$this->form_validation->set_rules('id_usuario_cp', 'id_usuario', 'trim|xss_clean|required');
				$this->form_validation->set_rules('password_new','Nueva Contrase&ntilde;a','trim|xss_clean|required|min_length[6]|max_length[20]|callback_no_permitir_nombre');			
				$this->form_validation->set_rules('password_confirm', 'Repetir Contrase&ntilde;a', 'trim|xss_clean|required|matches[password_new]');
				if($this->form_validation->run() === FALSE){
					$result['msj'] = $this->form_validation->_error_array;
					$result['exito'] = FALSE;
				}else{
					if ($this->usuarioModelo->cambiar_contrasena(sha1($this->input->post('password_new'))) === TRUE){
						$usuario = $this->input->post('usuario_cp');	
						$correo = $this->input->post('correoE_cp');	
						$password = $this->input->post('password_new');	
						$asunto = "Plan Operativo Anual - Acceso";
						$mensaje = "Sistema Plan Operativo Anual\n\n";
						$mensaje .= "Su contrase&ntilde;a ha sido modificada: \n\n";
						$mensaje .= "La nueva contrase&ntilde;a es: ".$password."\n";
						$mensaje .= "Para iniciar sesi&oacute;n, visit&eacute; el siguiente enlace:\n";	
						$mensaje .= base_url()."sesion/login\n\n\n";	
						$mensaje .= "Para m&aacute;s informaci&oacute;n puede contactar  a:\n\n";
						$mensaje .= "Unidad de Gobierno Electr&oacute;nico y Ciudad Digital\n";
						$mensaje .= "gobiernoelectronico@correo.culiacan.gob.mx\n";
						$mensaje .= "Conmutador 758-01-01 Ext. 1407\n";
						$mensaje .= "Directo 713-92-87\n";
						$this->load->library('email');
						$this->email->from("gobiernoelectronico@correo.culiacan.gob.mx", "Plan Operativo Anual");
						$this->email->to($correo);
						$this->email->subject($asunto);
						$this->email->message($mensaje);
						if (!$this->email->send()) {
							$result['exito'] = FALSE;
							$result['msj'] = $this->email->print_debugger();
						}else{
							$result['exito'] = TRUE;
							$result['msj'] = "Contrase&ntilde;a guardada satisfactoriamente.";
						}
					}else{
						$result['exito'] = FALSE;	
						$result['msj'] = "No se pudo cambiar la contrase&ntilde;a.";
					}
				}
				$this->output->set_header('Content-type: application/json');
				echo json_encode($result);
			}
		}
	}

	public function edicion(){		
		if($this->input->is_ajax_request() && $this->acl->proceso(32)){
			$this->load->library("form_validation");	
			$this->form_validation->set_rules('id_usuario', 'id_usuario', 'trim|xss_clean|required');
			if ($this->form_validation->run()){
				$result['usuario'] = $this->usuarioModelo->obtener_usuario();
			}
			echo json_encode($result);
		}
	}

	public function busqueda(){
		if($this->acl->proceso(33)){
			$filas = $this->usuarioModelo->filas_usuarios_busqueda($this->input->post('referencia'));
	       	$this->_paginacion($this->totalFilas,$filas,'usuario','paginacionUsu');
			$parametros['usuarios'] = $this->usuarioModelo->busqueda_usuario($this->input->post('referencia'),$this->totalFilas,$this->uri->segment(3));
			$this->load->view('seguridad/usuarios_tabla', $parametros);
		}
	}
	
	public function soloLetrasEspacios($string){        
		$string = utf8_encode($string);
		if (!(preg_match("/^([a-zA-Z����A������ ])+$/i", $string))){
			$this->form_validation->set_message('soloLetrasEspacios', 'El %s solo debe contener letras.');
            return FALSE;
        }else{
            return TRUE;
        }
	}

	public function password_old($password_old){
		if (sha1($password_old) === $this->usuarioModelo->obtener_contrasena()){
			return TRUE; 
		}else{
			$this->form_validation->set_message('password_old', 'La Contrase&ntilde;a Actual no es correcta.');
			return FALSE;
		}
	}
	
}
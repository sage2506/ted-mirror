<script type="text/javascript">
$(function() {
	$('#li-constancias').addClass('active');
	if ( ! $('ul.breadcrumb li.active').html() ) {
		$('ul.breadcrumb').append('<li class="active">Contancias</li>');
	};
	$('#btn-cancelar-constancia').click(function(){
		$('#contNombre').removeClass("error");
		$('#contPaterno').removeClass("error");
		$('#contNumero').removeClass("error");
		$('#contIdentificacion').removeClass("error");
		$('#cont-nueva').css({'display' : 'none'});
		$('#nva-identificacion').val('');
	});
	$("#identificacion").change(function(){
		if ( $("#identificacion").val() === 'OTRA' ) {
			$('#cont-nueva').css({'display' : 'block'});
		}else{
			if ( $("#incidencia").val() !== 'OTRA' ) {
				$('#cont-nueva').css({'display' : 'none'});
				$('#nva-identificacion').val('');
			};
		};
	});
	$('#btn-generar-constancia').click(function(){
		$('#btn-generar-constancia').button('loading');
		$.post(app.url + 'panel/registro_constancia', $('#form-constancia').serialize(), function(resultado){
			if ( resultado.exito === true ) {
				window.document.location = app.url + 'panel/crear_constancia/' + resultado.id_constancia;
				$.noticia('Se ha generado su Constancia.','success');
				$('#btn-cancelar-constancia').click();
			}else{
				if ( resultado.exito === false ) {
					first = true;
					$.each(resultado.mensaje, function(campo, aviso){
						if (first){
							$('div .error').removeClass('error');
							error = new Object({ campo: campo, mensaje: aviso });
							first = false;
						}
						$('#'+campo).parents('.control-group').addClass('error');
					});
					$.noticia(error.mensaje, 'danger');
					$('#'+error.campo).focus();
				};
			};
			$('#btn-generar-constancia').button('reset');
		},'json');
		return false;
	});
	$('body').on('click', 'button.btn-agregar', function() {
		$.post(app.url + 'panel/listado_constancias', function(data){
            $('#contenedor').html(data);
            $('#btn-ver-lista').attr('data-original-title','Ocultar Listado');
            $('#btn-ver-lista').removeClass('btn-agregar btn-success').addClass('btn-ocultar btn-danger').children('i').removeClass('icon-list-alt').addClass('icon-share-alt');
			$('#tbl-lista-constancias').slideDown();
	    	$('#form-constancia').slideUp();
        });
    });
    $('body').on('click', 'button.btn-ocultar', function() {
    	$('#btn-ver-lista').attr('data-original-title','Mostrar Listado');
        $('#btn-ver-lista').removeClass('btn-ocultar btn-danger').addClass('btn-agregar btn-success').children('i').removeClass('icon-share-alt').addClass('icon-list-alt');
		$('#tbl-lista-constancias').slideUp();
		$('#contenedor').html('');
    	$('#form-constancia').slideDown();
    });
});
</script>
<button id="btn-ver-lista" name="btn-ver-lista" class="btn-agregar btn btn-success pull-right opcion" title="Mostrar Listado" ><i class="icon-list-alt icon-white"></i></button>
<form id="form-constancia" name="form-constancia" class="form-horizontal" action="#" method="POST" onsubmit="return(false)" style="margin-top: 30px;" >
	<div class="row-fluid">
		<div class="span6">
			<div class="control-group" id="contNombre" >
				<label class="control-label" for="nombre">Nombre</label>
				<div class="controls">
					<input type="text" class="span8 conv-mayu" id="nombre" name="nombre" autofocus="autofocus" />
				</div>
			</div>
			<div class="control-group" id="contPaterno" >
				<label class="control-label" for="paterno">Apellido Paterno</label>
				<div class="controls">
					<input type="text" class="span8 conv-mayu" id="paterno" name="paterno" />
				</div>
			</div>
			<div class="control-group" id="contMaterno">
				<label class="control-label" for="materno">Apellido Materno</label>
				<div class="controls">
					<input type="text" class="span8 conv-mayu" id="materno" name="materno" />
				</div>
			</div>
		</div>
		<div class="span6">
			<div class="control-group" id="contIdentificacion" >
				<label class="control-label" for="identificacion">Identificación</label>
				<div class="controls">
					<select id="identificacion" name="identificacion">
						<option value="NULL">Seleccione</option>
						<option>IFE</option>
						<option>CURP</option>
						<option>PASAPORTE</option>
						<option>CARTILLA MILITAR</option>
						<option>OTRA</option>
					</select>
				</div>
			</div>
			<div class="control-group" id="cont-nueva" style="display:none">
				<div class="controls">
					<input type="text" class="span8 conv-mayu" id="nva-identificacion" name="nva-identificacion" placeholder="Especifique identificación"/>
				</div>
			</div>
			<div class="control-group" id="contNumero" >
				<label class="control-label" for="numero">Número</label>
				<div class="controls">
					<input type="text" class="span4 conv-mayu" id="numero" name="numero" />
				</div>
			</div>
		</div>
	</div>		
	<div class="form-actions" >
		<button id="btn-generar-constancia" name="btn-generar-constancia" class="btn btn-primary pull-right" data-loading-text="Espere.." ><i class="icon-file icon-white"></i> Expedir</button>
		<span class="pull-right">&nbsp;</span>
		<button type="reset" id="btn-cancelar-constancia" name="btn-cancelar-constancia" class="btn pull-right" >Cancelar</button>
	</div>
</form>
<div id="contenedor"></div>
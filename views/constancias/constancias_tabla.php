<span class="lead">Constancias Expedidas</span>
<hr>
<table id="tbl-lista-constancias" class="table table-striped table-hover table-condensed hide" >
	<thead>
		<tr>
			<th>NOMBRE COMPLETO</th>
			<th>IDENTIFICACIÓN</th>
			<th>NUMERO</th>
			<th>FECHA</th>
		</tr>
	</thead>
	<tbody>
		<?php if ( $constancias ): ?>
			<?php foreach ($constancias as $campo): ?>
				<tr>
					<td><?php echo $campo->nombre; ?> <?php echo $campo->paterno; ?> <?php echo $campo->materno; ?></td>
					<td><?php echo $campo->identificacion; ?></td>
					<td><?php echo $campo->numero; ?></td>
					<td><?php echo $campo->fecha; ?></td>
				</tr>
			<?php endforeach ?>
		<?php else: ?>
				<tr>
					<td colspan="4" >No se encontraron resultados.</td>
				</tr>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
$(function() {
	$('#ventanaRol').on('hidden', function () {
		$("#contRol").removeClass("error");
		$('#msjRol').html('');
		$('#rol').val('');
		$("#contDescripcion").removeClass("error");
		$('#msjDescripcion').html('');
		$('#descripcion').val('');
		$('#modo').val('0');
		$('#id_rol').val('');
		$('#titulo').html('Nuevo Rol');
	});
	$('#ventanaNew').click(function(){
		$('#ventanaRol').modal('show');
	});	
	$('#ventanaEliminar').on('hidden', function () {
		$('#idRol').val('');
	});
	$('#btnRol').click(function(){
		$('#btnRol').button('loading');
		$.post(app.url + 'rol/alta', $('#form_rol').serialize(), function(result){
			if (result.exito === true){
				$('#btnRol').button('reset');
				$('#ventanaRol').modal('hide');
				$.noticia(result.msj,'success');
                $refrescarNotificaciones();
			}else{
				$('#btnRol').button('reset');
				if (result.exito === false) {
					$.each(result.msj, function(campo,aviso){
						if (campo=='rol') {
							$('#contRol').addClass("error");
							$('#msjRol').html('<span class="help-inline">' + aviso + '</span>');
						}else{
							if (campo=='descripcion') {
								$('#contDescripcion').addClass("error");
								$('#msjDescripcion').html('<span class="help-inline">' + aviso + '</span>');
							};
						};
					});
				};
			};
		},'json');
		return false;
	});
	$('#btn-buscar').click(function(){
        $.post(app.url + 'rol/busqueda', $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$cambiarStatus = (function(el){
    	params = $(el).attr("id").split('-');
		id_rol = params[1];
		estado = $('span#estado-'+id_rol).html();
		$('span#estado-'+id_rol).removeClass().addClass("wait").html("");
		$.post(app.url + 'rol/cambiar_estado',{ id_rol : id_rol, estado : estado },
			function(result){
				if(result.success === true){
					$('span#estado-'+id_rol).removeClass("wait");
					if(result.estado == true){
						$.noticia(result.msj,'error');
						$('span#estado-'+id_rol).addClass("label label-important state");
						$('span#estado-'+id_rol).html("INACTIVO");
					}else{
						$.noticia(result.msj,'success');
						$('span#estado-'+id_rol).addClass("label label-success state");
						$('span#estado-'+id_rol).html("ACTIVO");						
					}
				}else{
					$.noticia(result.msj,'error');
				}
			}, "json"
		);
		return false;
    });
    $modalEliminar = (function(el){
        var id = $(el).attr('data-id');
		$.confirmar('¿Seguro que desea eliminar?',{ 
			aceptar: function(){
				$.post(app.url + 'rol/eliminar/'+id, function(result){
					if (result.exito === true){
						$.noticia(result.msj,'success');
						$refrescarNotificaciones();
					}else{
						if ( result.exito === false ) {
							$.noticia(result.msj,'error');
						};				
					};
				},'json');
			}
		});
    });
    $modalEdicion = (function(el){
    	$('#modo').val('1');
		$('#titulo').html('Edición de Rol');
		$('#id_rol').val($(el).attr('data-id-rol'));
		$('#rol').val($(el).attr('data-rol'));
		$('#descripcion').val($(el).attr('data-descripcion'));
		$('#ventanaRol').modal('show');
    });
    $refrescarNotificaciones = (function(){
        var filas = '<?php echo $this->totalFilas; ?>';
        var rango = 0;
        if ( $('div#paginacion ul li.active a').length ) {
            if ( $('div#paginacion ul li.active a').html() !== '') {
                rango = ($('div#paginacion ul li.active a').html() - 1) * filas; 
            };
        }else{
            rango = 0;
        };
        $.post(app.url + 'rol/paginacionRol/'+rango, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
    });
    $paginacion = (function(el){
        var parametros = ($(el).attr('href'));
        $.post(parametros, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$todasFunciones = (function(){
		$('div#paginacion ul li a.btn-paginar').on('click', function(e){
            e.preventDefault();
            $paginacion(this);
        });
	    $('table tbody tr span.btn-status').on('click',function() {
	        $cambiarStatus(this);
	    });
	    $('table tbody tr a.btn-eliminar').on('click',function() {
	        $modalEliminar(this);
	    });
	    $('table tbody tr a.btn-edicion').on('click',function() {
	        $modalEdicion(this);
	    });
	});
	$(document).ready(function() {
	    $todasFunciones();
	});
});
</script>
<form id="busqueda" class="form-search well" action="#" method="post">
	<div class="input-append">
		<input type="text" class="span4 search-query" placeholder="ingrese texto a buscar.." id="referencia" name="referencia" autofocus="autofocus" />
		<button type="submit" id="btn-buscar" class="btn btn-success input-small"><i class="icon-search icon-white"></i></button>
	</div>
	<button id="ventanaNew" type="button" class="btn btn-primary pull-right opcion" title="Nuevo Rol" ><i class="icon-plus-sign icon-white"></i></button>
</form>
<div id="contenedor">
	<table class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	        	<th width="2%">ID</th>	
	            <th width="36%">NOMBRE</th>
	            <th width="12%"></th>
	            <th width="40%">DESCRIPCIÓN</th>
	            <th width="5%">ESTADO</th>            
	        </tr>
	    </thead>
	    <tbody>
	    	<?php if ($roles): ?>
	    		<?php foreach ($roles as $campo): ?>
		    		<tr>
			        	<td><?=$campo->id_rol?></td>
			            <td><?=$campo->rol?></td>
			            <td>
			            	<a data-id='<?=$campo->id_rol?>' class="btn-eliminar btn btn-mini btn-danger pull-right opcion" title="Eliminar"><i class="icon-white icon-trash"></i></a>
			            	<span class="pull-right">&nbsp;</span>
				        	<a data-id-rol="<?=$campo->id_rol?>" data-rol="<?=$campo->rol?>" data-descripcion='<?=$campo->descripcion?>' class="btn-edicion btn btn-mini btn-info pull-right opcion" title="Editar"><i class="icon-white icon-edit"></i></a>
			            	<span class="pull-right">&nbsp;</span>
			            	<a class="btn btn-mini btn-warning pull-right opcion" title="Permisos" href="<?=base_url()?>rol/permisos/<?=$campo->id_rol?>"><i class="icon-white icon-th-list"></i></a>
			            </td>
			            <td><?=$campo->descripcion?></td>
			            <td>
			            <?php
			                switch($campo->estado){
			                    case 'activo': ?><span id="estado-<?=$campo->id_rol?>" class="label label-success btn-status">ACTIVO</span><?php
			                    	break;
			                    case 'inactivo': ?><span id="estado-<?=$campo->id_rol?>" class="label label-important btn-status">INACTIVO</span><?php
			                    	break;
			                }
			            ?>
			            </td>
			        </tr>
		    	<?php endforeach ?>
	    	<?php else: ?>
				<tr>
	    			<td colspan="5">No se encontraron resultados</td>
	    		</tr>
	    	<?php endif ?>
	    	
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
</div>
<div id="ventanaRol" class="modal hide fade">
	<div class="modal-header lead">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<span id="titulo">Nuevo Rol</span>
	</div>
	<div class="modal-body">
	    <form id="form_rol" action="#"  method="post" class="form-horizontal">    
	        <div class="clearfix">
	            <input type="hidden" id="modo" name="modo" value="0" />
	            <input type="hidden" id="id_rol" name="id_rol" />
	        </div>
	        <div class="control-group" id="contRol">
				<label class="control-label">Nombre</label>
				<div class="controls">	
			    	<input type="text" id="rol" name="rol"/>
			    	<span class="help-inline" id="msjRol"></span>
			     </div>
			</div>
			<div class="control-group" id="contDescripcion">
				<label class="control-label">Descripción</label>
				<div class="controls">	
			    	<textarea id="descripcion" name="descripcion" rows="3"></textarea>
			    	<span class="help-inline" id="msjDescripcion"></span>
			     </div>
			</div>        
	    </form>
	</div>
    <div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal" >Cancelar</a>
		<button id="btnRol" name="btnRol" class="btn btn-primary" type="submit" data-loading-text="Espere..">Guardar</button>
	</div>
</div>
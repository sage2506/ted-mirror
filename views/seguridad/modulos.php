<script type="text/javascript">    
$(function() {
	$('#ventanaModulo').on('hidden', function () {
		$("#contModulo").removeClass("error");
		$('#msjModulo').html('');
		$('#modulo').val('');
		$('#id_modulo').val('');
		$('#modo').val('0');
		$('#titulo').html('Nuevo Módulo');
	});
	$('#ventanaNew').click(function(){
		$('#ventanaModulo').modal('show');
	});
	$('#btnModulo').click(function(){
		$('#btnModulo').button('loading');
		$.post(app.url + 'modulo/alta', $('#form_modulo').serialize(), function(result){
			if (result.exito === true){
				$('#btnModulo').button('reset');
				$('#ventanaModulo').modal('hide');
				$.noticia(result.msj,'success');
                $refrescarNotificaciones();
			}else{
				if (result.exito === false) {
					$('#btnModulo').button('reset');
					$('#contModulo').addClass("error");
					$('#msjModulo').html('<span class="help-inline">' + result.msj + '</span>');
				};
			};
		},'json');
		return false;
	});
	$('#btn-buscar').click(function(){
        $.post(app.url + 'modulo/busqueda', $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$modalEdicion = (function(el){
		$('#modo').val('1');
		$('#titulo').html('Edición Módulo');
		$('#id_modulo').val($(el).attr('data-id-modulo'));
		$('#modulo').val($(el).attr('data-modulo'));
		$('#ventanaModulo').modal('show');
    });
    $modalEliminar = (function(el){
        var id = $(el).attr('data-id-modulo');
		$.confirmar('¿Seguro que desea eliminar?',{ 
			aceptar: function(){
				$.post(app.url + 'modulo/eliminar/'+id, function(result){
					if (result.exito === true){
						$.noticia(result.msj,'success');
						$refrescarNotificaciones();
					}else{
						if ( result.exito === false ) {
							$.noticia(result.msj,'error');
						};
					};
				},'json');
			}
		});
    });
    $refrescarNotificaciones = (function(){
        var filas = '<?php echo $this->totalFilas; ?>';
        var rango = 0;
        if ( $('div#paginacion ul li.active a').length ) {
            if ( $('div#paginacion ul li.active a').html() !== '') {
                rango = ($('div#paginacion ul li.active a').html() - 1) * filas; 
            };
        }else{
            rango = 0;
        };
        $.post(app.url + 'modulo/paginacionMod/'+rango, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
    });
    $paginacion = (function(el){
        var parametros = ($(el).attr('href'));
        $.post(parametros, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$todasFunciones = (function(){
		$('div#paginacion ul li a.btn-paginar').on('click', function(e){
            e.preventDefault();
            $paginacion(this);
        });
	    $('table tbody tr a.btn-eliminar').on('click',function() {
	        $modalEliminar(this);
	    });
	    $('table tbody tr a.btn-edicion').on('click',function() {
	        $modalEdicion(this);
	    });
	});
	$(document).ready(function() {
	    $todasFunciones();
	});
});  
</script>
<form id="busqueda" class="form-search well" action="#" method="post" >
	<div class="input-append">
		<input type="text" class="span4 search-query" placeholder="ingrese texto a buscar.." id="referencia" name="referencia" autofocus="autofocus"/>
		<button type="submit" id="btn-buscar" class="btn btn-success input-small"><i class="icon-search icon-white"></i></button>
	</div>
    <button type="button" id="ventanaNew" class="btn btn-primary opcion pull-right" title="Nuevo Módulo"><i class="icon-plus-sign icon-white"></i></button>
</form>
<div id="contenedor">
	<?php echo $tabla; ?>
</div>
<div id="ventanaModulo" class="modal hide fade">
	<div class="modal-header lead">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<span id="titulo">Nuevo Módulo</span>
	</div>
	<div class="modal-body">
        <form id="form_modulo" action="#"  method="post" class="form-horizontal">    
            <div class="clearfix">
                <input type="hidden" id="modo" name="modo" value="0" />
                <input type="hidden" id="id_modulo" name="id_modulo" />
            </div>
            <div class="control-group" id="contModulo">
				<label class="control-label">Nombre</label>
				<div class="controls">	
			    	<input type="text" id="modulo" name="modulo" size="40" />
			    	<span class="help-inline" id="msjModulo"></span>
			     </div>
			</div>   
        </form>
    </div>
    <div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal" >Cancelar</a>
		<button id="btnModulo" name="btnModulo" class="btn btn-primary" type="submit" data-loading-text="Espere..">Guardar</button>
	</div>
</div>
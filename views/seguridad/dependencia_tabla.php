<script type="text/javascript">
$(function() {
	$('.opcion').tooltip();
});
</script>
<table class="table table-striped table-hover table-condensed">
	    <thead>
			<tr>
				<th>NOMBRE</th>
				<th style="width:10px"></th>
			</tr>
	    </thead>
	    <tbody>
	    	<?php if ($dependencias): ?>
	    		<?php foreach ($dependencias as $campo): ?>
					<tr>
				        <td align="left"><?=$campo->dependencia?></td>
				        <td>
				        	<a class="btn btn-mini btn-info btnEdicion opcion" title="Editar" data-toggle="modal" href="#ventanaNuevo" rel="tooltip" data-dependencia='<?=$campo->id_dependencia?>' data-padre="<?=$campo->parent_id?>"><i class="icon-white icon-edit"></i></a>
				        </td>            
				    </tr> 
				<?php endforeach ?>
	    	<?php else: ?>
	    		<tr>
	    			<td colspan="2">No se encontraron resultados</td>
	    		</tr>
	    	<?php endif ?>
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
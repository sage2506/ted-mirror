<script type="text/javascript">
$(function() {
	$('#li-modulo').addClass('active');
	$('ul.breadcrumb').append('<li class="active"><a>Seguridad</a><span class="divider">/</span></li><li class="active"><a>Módulos</a><span class="divider">/</span></li><li class="active">Procesos</li>');
	$('#ventanaProceso').on('hidden', function () {
		$("#contNombreProceso").removeClass("error");
		$('#msjNombreProceso').html('');
		$("#contProceso").removeClass("error");
		$('#msjProceso').html('');
		$('#id_proceso').val('');
		$('#modo').val('0');
		$('#titulo').html('Nuevo Proceso');
		$('#nombre_proceso').val('');
		$('#proceso').val('');
	});
	$('#ventanaNew').click(function(){
		$('#ventanaProceso').modal('show');
	});
	$('#btnProceso').click(function(){
		$('#btnProceso').button('loading');
		$.post(app.url + 'proceso/alta', $('#form_proceso').serialize(), function(result){
			if (result.exito === true){
				$('#btnProceso').button('reset');
				$('#ventanaProceso').modal('hide');
				$.noticia(result.msj,'success');
				$refrescarNotificaciones();
			}else{
				if (result.exito === false) {
					$('#btnProceso').button('reset');
					$.each(result.msj, function(campo,aviso){
						if (campo=='nombre_proceso') {
							$('#contNombreProceso').addClass("error");
							$('#msjNombreProceso').html('<span class="help-inline">' + aviso + '</span>');
						}else{
							if (campo=='proceso') {
								$('#contProceso').addClass("error");
								$('#msjProceso').html('<span class="help-inline">' + aviso + '</span>');
							};
						};
					});
				};
			};
		},'json');
		return false;
	});
	$('#btn-buscar').click(function(){
        $.post(app.url + 'proceso/busqueda', $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$modalEliminar = (function(el){
        var id = $(el).attr('data-id-proceso');
		$.confirmar('¿Seguro que desea eliminar?',{ 
			aceptar: function(){
				$.post(app.url + 'proceso/eliminar/'+id, function(result){
					if (result.exito === true){
						$.noticia(result.msj,'success');
						$refrescarNotificaciones();
					}else{
						if ( result.exito === false ) {
							$.noticia(result.msj,'error');
						};
					};
				},'json');
			}
		});
    });
    $modalEdicion = (function(el){
		$('#modo').val('1');
		$('#titulo').html('Edición Proceso');
		$('#id_proceso').val($(el).attr('data-id-proceso'));
		$('#id_modulo').val($(el).attr('data-id-modulo'));
		$('#nombre_proceso').val($(el).attr('data-nombre-proceso'));
		$('#proceso').val($(el).attr('data-proceso'));
		$('#ventanaProceso').modal('show');
    });
    $refrescarNotificaciones = (function(){
        var filas = '<?php echo $this->totalFilas; ?>';
        var rango = 0;
        if ( $('div#paginacion ul li.active a').length ) {
            if ( $('div#paginacion ul li.active a').html() !== '') {
                rango = ($('div#paginacion ul li.active a').html() - 1) * filas; 
            };
        }else{
            rango = 0;
        };
        $.post(app.url + 'proceso/paginacionPro/'+rango, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
    });
    $paginacion = (function(el){
        var parametros = ($(el).attr('href'));
        $.post(parametros, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$todasFunciones = (function(){
		$('div#paginacion ul li a.btn-paginar').on('click', function(e){
            e.preventDefault();
            $paginacion(this);
        });
	    $('table tbody tr a.btn-eliminar').on('click',function() {
	        $modalEliminar(this);
	    });
	    $('table tbody tr a.btn-edicion').on('click',function() {
	        $modalEdicion(this);
	    });
	});
	$(document).ready(function() {
	    $todasFunciones();
	});
});
</script>
<?php foreach ($modulo as $campo): ?>
<p class="lead"><a class="btn btn-small opcion" href="<?=base_url()?>modulo/listado" title="Volver a Módulos" data-placement="right"><i class="icon-arrow-left"></i></a>&nbsp;&nbsp;PROCESOS DEL MÓDULO <?=strtoupper($campo->nombre_modulo)?></p>
<?php endforeach ?>
<form id="busqueda" class="form-search well" action="#" method="post" >
	<input type="hidden" id="idModulo" name="idModulo" value="<?php echo $idModulo; ?>" />
	<div class="input-append">
		<input type="text" class="span4 search-query" placeholder="ingrese texto a buscar.." id="referencia" name="referencia" autofocus="autofocus"/>
		<button type="submit" id="btn-buscar" class="btn btn-success input-small"><i class="icon-search icon-white"></i></button>
	</div>
    <button type="button" id="ventanaNew" class="btn btn-primary opcion pull-right" title="Nuevo Proceso"><i class="icon-plus-sign icon-white"></i></button>
</form>
<div id="contenedor">
	<table class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	        	<th width="3%">ID</th>
	            <th width="45%">NOMBRE DEL PROCESO</th>
	            <th width="42%">PROCESO</th>                
	            <th width="10%"></th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php if ($procesos): ?>
	    		<?php foreach ($procesos as $key => $campo): ?>
		        	<tr>
			        	<td><?=$campo->id_proceso?></td>
			            <td><?=$campo->nombre_proceso?></td>
			            <td><?=$campo->proceso?></td>
			            <td>
			            	<span class="pull-right">&nbsp;</span>	
							<a data-id-proceso='<?=$campo->id_proceso?>' class="btn-eliminar btn btn-mini btn-danger pull-right opcion" title="Eliminar"><i class="icon-white icon-trash"></i></a>
			            	<span class="pull-right">&nbsp;</span>
				        	<a data-id-proceso="<?=$campo->id_proceso?>" data-id-modulo="<?=$campo->id_modulo?>" data-nombre-proceso="<?=$campo->nombre_proceso?>" data-proceso="<?=$campo->proceso?>" class="btn-edicion btn btn-mini btn-info pull-right opcion" title="Editar"><i class="icon-white icon-edit"></i></a>
			            </td>            
			        </tr>
		        <?php endforeach ?>
	    	<?php else: ?>
	    			<tr>
		    			<td colspan="4">No se encontraron resultados.</td>
		    		</tr>
	    	<?php endif ?>   
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
</div>
<div id="ventanaProceso" class="modal hide fade">
	<div class="modal-header lead">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<span id="titulo">Nuevo Proceso</span>
	</div>
	<div class="modal-body">
	    <form id="form_proceso" action="#"  method="post" class="form-horizontal">    
	        <div class="clearfix">
	            <input type="hidden" id="modo" name="modo" value="0" />
	            <input type="hidden" id="id_modulo" name="id_modulo" value="<?=$idModulo?>" />
	            <input type="hidden" id="id_proceso" name="id_proceso" />
	        </div>
	        <div class="control-group" id="contNombreProceso">
	            <label class="control-label"><b>Nombre Proceso</b></label>
	            <div class="controls">
	            	<input type="text" id="nombre_proceso" name="nombre_proceso" size="40" maxlength="255" />
	            	<span class="help-inline" id="msjNombreProceso"></span>
	            </div>
	        </div>  
	        <div class="control-group" id="contProceso">
	            <label class="control-label"><b>Proceso</b></label>
	            <div class="controls">
	            	<input type="text" id="proceso" name="proceso" size="40" />
	            	<span class="help-inline" id="msjProceso"></span>
	            </div>
	        </div>     
	    </form>
	</div>
    <div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal" >Cancelar</a>
		<button id="btnProceso" name="btnProceso" class="btn btn-primary" type="submit" data-loading-text="Espere..">Guardar</button>
	</div>
</div>
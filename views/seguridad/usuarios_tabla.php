<script type="text/javascript">
$(function() {
	$('.opcion').tooltip();
});
</script>
<table id="tabla-usuarios" class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	        	<th width="2%">ID</th>
	        	<th width="8%">USUARIO</th>
	            <th width="12%"></th>
	            <th width="10%">GRUPO</th>
	            <th width="8%">NOMBRE</th>
	            <th width="10%">CORREO</th>
	            <th width="5%">ESTADO</th>
	        </tr>
	    </thead>
	    <tbody>
		<?php if ($usuarios): ?>
			<?php foreach ($usuarios as $campo): ?>
		    	<tr>
		        	<td><?=$campo->id_usuario?></td>
		            <td><?=$campo->usuario?></td>
		            <td>
		            	<a data-id='<?=$campo->id_usuario?>' class="btn btn-danger btn-mini btn-eliminar pull-right opcion" title="Eliminar"><i class="icon-trash icon-white"></i></a>
		            	<span class="pull-right">&nbsp;</span>
			        	<a data-id-usuario='<?=$campo->id_usuario?>' data-usuario="<?=$campo->usuario?>" data-correo="<?=$campo->correo?>" class="btn btn-inverse btn-mini btn-cambiar-password pull-right opcion" title="Cambiar Contrase&ntilde;a"><i class="icon-lock icon-white cambiarContrasena"></i></a>
			        	<span class="pull-right">&nbsp;</span>
			        	<a data-id-usuario='<?=$campo->id_usuario?>' class="btn btn-info btn-mini btn-editar pull-right opcion" title="Editar"><i class="icon-edit icon-white"></i></a>	
		            </td>
		            <td><?=$campo->rol?></td>
		            <td><?=$campo->nombre?></td>
		            <td><?=$campo->correo?></td>
		            <td>
		            <?php
		                switch($campo->estado){
		                    case 'ACTIVO': ?><span id="estado-<?=$campo->id_usuario?>" class="label label-success btn-status">ACTIVO</span><?php
		                    	break;
		                    case 'INACTIVO': ?><span id="estado-<?=$campo->id_usuario?>" class="label label-important btn-status">INACTIVO</span><?php
		                    	break;
		                }
		            ?>                        
		            </td>
		        </tr> 
		    <?php endforeach ?>
		<?php else: ?>
				<tr>
	    			<td colspan="8">No se encontraron resultados</td>
	    		</tr>
		<?php endif ?>
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
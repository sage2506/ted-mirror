<script type="text/javascript">
$(function() {
	$('.opcion').tooltip();
});
</script>
<table class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	        	<th width="2%">ID</th>	
	            <th width="36%">NOMBRE</th>
	            <th width="12%"></th>
	            <th width="40%">DESCRIPCIÓN</th>
	            <th width="5%">ESTADO</th>            
	        </tr>
	    </thead>
	    <tbody>
	    	<?php if ($roles): ?>
	    		<?php foreach ($roles as $campo): ?>
		    		<tr>
			        	<td><?=$campo->id_rol?></td>
			            <td><?=$campo->rol?></td>
			            <td>
			            	<a data-id='<?=$campo->id_rol?>' class="btn-eliminar btn btn-mini btn-danger pull-right opcion" title="Eliminar"><i class="icon-white icon-trash"></i></a>
			            	<span class="pull-right">&nbsp;</span>
				        	<a data-id-rol="<?=$campo->id_rol?>" data-rol="<?=$campo->rol?>" data-descripcion='<?=$campo->descripcion?>' class="btn-edicion btn btn-mini btn-info pull-right opcion" title="Editar"><i class="icon-white icon-edit"></i></a>
			            	<span class="pull-right">&nbsp;</span>
			            	<a class="btn btn-mini btn-warning pull-right opcion" title="Permisos" href="<?=base_url()?>rol/permisos/<?=$campo->id_rol?>"><i class="icon-white icon-th-list"></i></a>
			            </td>
			            <td><?=$campo->descripcion?></td>
			            <td>
			            <?php
			                switch($campo->estado){
			                    case 'activo': ?><span id="estado-<?=$campo->id_rol?>" class="label label-success btn-status">ACTIVO</span><?php
			                    	break;
			                    case 'inactivo': ?><span id="estado-<?=$campo->id_rol?>" class="label label-important btn-status">INACTIVO</span><?php
			                    	break;
			                }
			            ?>
			            </td>
			        </tr>
		    	<?php endforeach ?>
	    	<?php else: ?>
				<tr>
	    			<td colspan="5">No se encontraron resultados</td>
	    		</tr>
	    	<?php endif ?>
	    	
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
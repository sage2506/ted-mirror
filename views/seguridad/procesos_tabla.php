<script type="text/javascript">
$(function() {
	$('.opcion').tooltip();
});
</script>
<table class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	        	<th width="3%">ID</th>
	            <th width="45%">NOMBRE DEL PROCESO</th>
	            <th width="42%">PROCESO</th>                
	            <th width="10%"></th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php if ($procesos): ?>
	    		<?php foreach ($procesos as $key => $campo): ?>
		        	<tr>
			        	<td><?=$campo->id_proceso?></td>
			            <td><?=$campo->nombre_proceso?></td>
			            <td><?=$campo->proceso?></td>
			            <td>
			            	<span class="pull-right">&nbsp;</span>	
							<a data-id-proceso='<?=$campo->id_proceso?>' class="btn-eliminar btn btn-mini btn-danger pull-right opcion" title="Eliminar"><i class="icon-white icon-trash"></i></a>
			            	<span class="pull-right">&nbsp;</span>
				        	<a data-id-proceso="<?=$campo->id_proceso?>" data-id-modulo="<?=$campo->id_modulo?>" data-nombre-proceso="<?=$campo->nombre_proceso?>" data-proceso="<?=$campo->proceso?>" class="btn-edicion btn btn-mini btn-info pull-right opcion" title="Editar"><i class="icon-white icon-edit"></i></a>
			            </td>            
			        </tr>
		        <?php endforeach ?>
	    	<?php else: ?>
	    			<tr>
		    			<td colspan="4">No se encontraron resultados.</td>
		    		</tr>
	    	<?php endif ?>   
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
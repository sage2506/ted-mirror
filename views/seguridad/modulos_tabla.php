<script type="text/javascript">
$(function() {
	$('.opcion').tooltip();
});
</script>
<table class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	        	<th width="3%">ID</th>
	            <th>NOMBRE DEL MÓDULO</th>
	            <th width="8%">PROCESOS</th>
	            <th width="12%"></th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php if ($modulos): ?>
	    		<?php foreach ($modulos as $campo): ?>
		    	<tr>
		        	<td><?=$campo->id_modulo?></td>
		            <td><?=$campo->modulo?></td>
		            <td><?=$campo->numero_procesos?></td>
		            <td>
		            	<span class="pull-right">&nbsp;</span>
						<a data-id-modulo='<?=$campo->id_modulo?>' class="btn-eliminar btn btn-mini btn-danger pull-right opcion" title="Eliminar"><i class="icon-white icon-trash"></i></a>
						<span class="pull-right">&nbsp;</span>
						<a data-id-modulo="<?=$campo->id_modulo?>" data-modulo='<?=$campo->modulo?>' class="btn-edicion btn btn-mini btn-info pull-right opcion" title="Editar"><i class="icon-white icon-edit"></i></a>
						<span class="pull-right">&nbsp;</span>
						<a class="btn btn-mini btn-inverse pull-right opcion" title="Procesos" href="<?=base_url()?>modulo/proceso/<?=$campo->id_modulo?>" ><i class="icon-white icon-cog"></i></a>
		            </td>            
		        </tr>
		    	<?php endforeach ?> 
	    	<?php else: ?>
	    		<tr>
	    			<td colspan="4">No se encontraron resultados.</td>
	    		</tr>
	    	<?php endif ?>
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
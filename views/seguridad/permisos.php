<script type="text/javascript">    
var id_rol, id_modulo, id_proceso, modo;
$(function() {
	$('#li-rol').addClass('active');
	$('ul.breadcrumb').append('<li class="active"><a>Seguridad</a><span class="divider">/</span></li><li class="active"><a>Roles</a><span class="divider">/</span></li><li class="active">Permisos</li>');
	$permisosModulos = (function(el){
		id_rol = $('#idRol').val();
		var parametros = ($(el).attr('rel')).split('|');
		id_modulo = parametros[0];
		id_proceso = parametros[1];
		$.post(app.url + 'rol/cambiar_permiso',{ id_rol : id_rol, id_modulo : id_modulo, id_proceso : id_proceso},function(result){
			if(result.success === true){
				$('div#primerDiv div#l'+id_modulo+' ul li a span#proceso-'+id_proceso+' span#marca-'+id_proceso).html(result.modo);
			}else{
				$.noticia(result.msg,'error');
			}
		},"json");
		return false;
	});
	$(document).ready(function() {
	    $('div.tab-pane ul li a.modPermiso').on('click',function(e) {
	    	e.preventDefault();
	        $permisosModulos(this);
	    });
	});	
});    
</script>
<p class="lead"><a class="btn btn-small opcion" href="<?=base_url()?>rol/listado" title="Volver a Roles" data-placement="right"><i class="icon-arrow-left"></i></a>&nbsp;&nbsp;PERMISOS DE <?=strtoupper($nombre_rol)?></p>
<!--<form id="busqueda" class="form-search well" action="#" method="post" >
	<input type="hidden" id="idRol" name="idRol" value="<?=$id_rol?>" />
	<div class="input-append">
		<input type="text" class="span4 search-query" placeholder="ingrese texto a buscar.." id="referencia" name="referencia" autofocus="autofocus"/>
		<button type="submit" id="btn-buscar" class="btn btn-success input-small"><i class="icon-search icon-white"></i></button>
	</div>
</form>-->
<input type="hidden" id="idRol" name="idRol" value="<?=$id_rol?>" />
<div class="tabbable tabs-left">
<?php
echo '<ul class="nav nav-tabs">';
foreach($menu as $indice_modulo => $valores_modulos) {
	foreach($valores_modulos as $nombre_modulo => $valores_procesos) {
		echo '<li>
				<span id="modulo-'.$indice_modulo.'" ></span>
				<a href="#l'.$indice_modulo.'" data-toggle="tab"> '.strtoupper($nombre_modulo).'</a>
			  </li>';
	}
}
echo "</ul>";
echo '<div id="primerDiv" class="tab-content" style="width:auto">
<span id="tituloPrincipal"></span>';
foreach($menu as $indice_modulo => $valores_modulos) {
	foreach($valores_modulos as $nombre_modulo => $valores_procesos) {
		echo '<div class="tab-pane" id="l'.$indice_modulo.'"><h4 class="lead" style="text-align:center;">'.strtoupper($nombre_modulo).'</h4>';
		foreach($valores_procesos as $indice_proceso => $valores_permisos) {
			echo '<ul class="unstyled">';
			foreach($valores_permisos as $nombre_proceso => $permiso) {
				if($permiso == 1){
					echo '<li id="modo-'.$permiso.'">
							<a href="#" style="text-decoration:none;color:#333" class="modPermiso" rel="'.$indice_modulo.'|'.$indice_proceso.'">
								<span id="proceso-'.$indice_proceso.'">
									<span id="marca-'.$indice_proceso.'">
										<span class="badge badge-success">
											<i class="icon-ok icon-white"></i>
										</span>
									</span> '.$nombre_proceso.'
								</span>
							</a>';
				}else{
					echo '<li id="modo-'.$permiso.'">
							<a href="#" style="text-decoration:none;color:#333" class="modPermiso" rel="'.$indice_modulo.'|'.$indice_proceso.'">
								<span id="proceso-'.$indice_proceso.'">
									<span id="marca-'.$indice_proceso.'">
										<span class="badge badge-important">
											<i class="icon-remove icon-white"></i>
										</span>
									</span> '.$nombre_proceso.'
								</span>
							</a>';
				}
			}
			echo '</ul>';
		}
		echo "</div>";
	}
}
echo '</div>';
?>
</div>
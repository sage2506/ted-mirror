<?php if (! defined('BASEPATH')) exit('No direct script access'); ?>
<script type="text/javascript" charset="utf-8" src="<?=base_url()?>js/passwordstrength.min.js"></script>
<script type="text/javascript">
$(function() { 
	$('.fuerzaMsg').val('Seguridad de la Contraseña');
	$('.fuerzaPass').css({'background-color': '#CCC','color':'#4F4F4F'});
	$('#password, #password_new').on( 'keyup', function() {
		var fuerza = passwordStrength($(this).val())
		switch(fuerza){
			case 0:
				$('.fuerzaMsg').val('Seguridad de la contraseña').css({'color':'#4F4F4F'});
				$('.fuerzaPass').css({'background-color': '#CCC','color':'#4F4F4F'});
			break;
			case 1:
				$('.fuerzaPass').css({'background-color' : '#ff6600', 'color':'#FFF'});
				$('.fuerzaMsg').val('Débil');
				break;
			case 2:
				$('.fuerzaPass').css({'background-color' : 'red', 'color':'#FFF'});
				$('.fuerzaMsg').val('Corta');
				break;
			case 3:
				$('.fuerzaPass').css({'background-color' : '#6699cc', 'color':'#FFF'});
				$('.fuerzaMsg').val('Media');
				break;
			case 4:
				$('.fuerzaPass').css({'background-color' : '#9dc425', 'color':'#FFF'});
				$('.fuerzaMsg').val('Fuerte');
				break;
			default :
				$('.fuerzaPass').css({'background-color' : '#555', 'color':'#FFF'});
		}
	}).focusout( function() {
		$( this ).trigger( 'keyup' );
	});
	$('#loading').hide();
    $('#correo').blur(function(){
		var a = $("#correo").val();
		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
		if(filter.test(a)){
			$('#loading').show();
			$.post(app.url + 'usuario/comprobar_correo', {
				correo: $('#correo').val()
			}, function(response){
				$('#loading').hide();
				if(response.exito == true){
					$('#msjCorreo_new').html('');
				}else{
					$('#newCorreo').addClass("error");
					setTimeout("finishAjax('msjCorreo_new', '"+response.msj+"')", 400);					
				}
			});
		}
		return false;
	});
	$('#ventanaCambiarContrasena').on('hidden', function () {
		$("#np_password").removeClass("error");
		$("#np_Rpassword").removeClass("error");
		$('#msjPass_np').html('');
		$('#msjPassR_np').html('');
		$('#password_new').val('');
		$('#password_confirm').val('');
		$('#id_usuario_cp').val('');
		$('#usuario_cp').val('');
		$('#correoE_cp').val('');
		$('.fuerzaMsg').val('Seguridad de la contraseña').css({'color':'#4F4F4F'});
		$('.fuerzaPass').css({'background-color': '#CCC','color':'#4F4F4F'});
	});
	$mostrarForm = (function(){
    	$('#ventanaNew').removeClass('btn-agregar btn-primary').addClass('btn-ocultar btn-danger').children('i').removeClass('icon-ok').addClass('icon-minus-sign');
    	$('#form_usuario').slideDown();
    	$('#tabla-usuarios').slideUp();
    	$('div#paginacion').css({'display' : 'none'});
    	$('#referencia').val('').attr('disabled',true);
	    $('#btn-buscar').attr('disabled',true);
    	$('#nombre').focus();
    });
    $("form#busqueda").on('click', 'button.btn-agregar', function(){
    	$mostrarForm();
    	$('#lbl-accion').html('Nuevo').addClass('badge-success');
    });
    $ocultarForm = (function(){    	
		$('#form_usuario').slideUp();
		$('#tabla-usuarios').slideDown();
		$('div#paginacion').css({'display' : 'block'});
		$('#ventanaNew').removeClass('btn-ocultar btn-danger').addClass('btn-agregar btn-primary').children('i').removeClass('icon-minus-sign').addClass('icon-ok');
    	$('#parent_id').val('');
    	$('#usuario').attr('disabled',false);
    	$('#contPass').css({'display':'block'});
		$('#newPassword').css({'display':'block'});
		$('#newR_password').css({'display':'block'});    	
		$('.fuerzaMsg').val('Seguridad de la Contraseña').css({'color':'#4F4F4F'});
		$('.fuerzaPass').css({'background-color': '#CCC','color':'#4F4F4F'});
		$('#referencia').attr('disabled',false).focus();
	    $('#btn-buscar').attr('disabled',false);
	    $('#msjCorreo_new').html('');
	    $('#lbl-accion').html('').removeClass('badge-success');
	    $("#newNombre").removeClass("error");
		$("#newCorreo").removeClass("error");
		$("#newGrupo").removeClass("error");
		$("#newUsuario").removeClass("error");
		$("#newPassword").removeClass("error");
		$("#newR_password").removeClass("error");
		$("#newPaterno").removeClass("error");
		$("#newDependencia").removeClass("error");
		$("#newCargo").removeClass("error");
		$('#msjNombre_new').html('');
		$('#msjCorreo_new').html('');
		$('#msjGrupo_new').html('');
		$('#msjUsuario_new').html('');
		$('#msjPass_new').html('');
		$('#msjPassR_new').html('');
		$('#msjPaterno').html('');
		$('#msjDependencia').html('');
		$('#msjCargo').html('');
		$('#correo').val('');
		$('#nombre').val('');
		$('#id_rol').val('');
		$('#usuario').val('');
		$('#password').val('');
		$('#confirm_password').val('');
		$('#modo').val('0');
		$('#id_usuario').val('');
		document.getElementById('form_usuario').reset();
    });
    $('form#busqueda').on('click', 'button.btn-ocultar', function(){
		$ocultarForm();
	});
	$('#btn-cancelar-usuario').click(function(){
		$ocultarForm();
	});
	$('#nuevoUsuario').click(function(){
		$('#nuevoUsuario').button('loading');
		$.post(app.url + 'usuario/alta', $('#form_usuario').serialize(), function(resultado){
			if (resultado.exito === true){
				$('#nuevoUsuario').button('reset');
				$ocultarForm();
				$.noticia(resultado.msj,'success');
                $refrescarNotificaciones();
			}else{
				$('#nuevoUsuario').button('reset');
				if (resultado.exito === false) {
					$.each(resultado.msj, function(campo,aviso){
						if (campo=='nombre') {
							$('#newNombre').addClass("error");
							$('#msjNombre_new').html('<span class="help-inline">' + aviso + '</span>');
						}else{
							if (campo=='id_rol') {
								$('#newGrupo').addClass("error");
								$('#msjGrupo_new').html('<span class="help-inline">' + aviso + '</span>');
							}else{
								if (campo=='usuario') {
									$('#newUsuario').addClass("error");
									$('#msjUsuario_new').html('<span class="help-inline">' + aviso + '</span>');
								}else{
									if (campo=='password') {
										$('#newPassword').addClass("error");
										$('#msjPass_new').html('<span class="help-inline">' + aviso + '</span>');
									}else{
										if (campo=='confirm_password') {
											$('#newR_password').addClass("error");
											$('#msjPassR_new').html('<span class="help-inline">' + aviso + '</span>');
										}else{
											if (campo=='correo') {
												$('#newCorreo').addClass("error");
												$('#msjCorreo_new').html('<span class="help-inline">' + aviso + '</span>');
											}else{
												if (campo=='apellido_paterno') {
													$('#newPaterno').addClass("error");
													$('#msjPaterno').html('<span class="help-inline">' + aviso + '</span>');
												}else{
													if (campo=='parent_id') {
														$('#newDependencia').addClass("error");
														$('#msjDependencia').html('<span class="help-inline">' + aviso + '</span>');
													}else{
														if (campo=='cargo') {
															$('#newCargo').addClass("error");
															$('#msjCargo').html('<span class="help-inline">' + aviso + '</span>');
														};
													};
												};
											};
										};
									};
								};
							};
						};
					});
				};
			};
		},'json');
		return false;		
	});
	$('#nuevaPass').click(function(){
		$('#nuevaPass').button('loading');
		$.post(app.url + 'usuario/cambiarContrasena',$('#form_cambiar').serialize(),function(resultado){
			if (resultado.exito === true) {
				$('#nuevaPass').button('reset');
				$('#ventanaCambiarContrasena').modal('hide');
				$.noticia(resultado.msj,'success');
                $refrescarNotificaciones();
			}else{
				$('#nuevaPass').button('reset');
				if (resultado.exito === false) {
					$.each(resultado.msj, function(campo,aviso){
						if (campo=='id_usuario_cp') {
							$('#newAviso').addClass("error");
							$('#msjAviso_new').html('<span class="help-inline">' + aviso + '</span>');
						}else{
							if (campo=='password_new') {
								$('#np_password').addClass("error");
								$('#msjPass_np').html('<span class="help-inline">' + aviso + '</span>');
							}else{
								if (campo=='password_confirm') {
									$('#np_Rpassword').addClass("error");
									$('#msjPassR_np').html('<span class="help-inline">' + aviso + '</span>');
								}else{
									$('#newAviso').addClass("error");
									$('#msjAviso_new').html('<span class="help-inline">' + aviso + '</span>');
								};
							};
						};
					});
				};
			};
		},'json');
		return false;
	});
	$('#btn-buscar').click(function(){
        $.post(app.url + 'usuario/busqueda', $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
    $modalEdicion = (function(el){
    	$.post(app.url + 'usuario/edicion',{ id_usuario : $(el).attr('data-id-usuario') },function(resultado){
			var editar = resultado.usuario;
			$('#modo').val('1');
			$('#id_usuario').val(editar.id_usuario);
			$('#nombre').val(editar.nombre);
			$('#correo').val(editar.correo);
			$('#id_rol').val(editar.id_rol);
			$('#usuario').val(editar.usuario).attr('disabled',true);
			$('#contPass').css({'display':'none'});
			$('#newPassword').css({'display':'none'});
			$('#newR_password').css({'display':'none'});
			$('#lbl-accion').html('Editar').addClass('badge-warning');
			$mostrarForm();
		},'json');
		return false;
    });
	$modalEliminar = (function(el){
        var id = $(el).attr('data-id');
		$.confirmar('¿Seguro que desea eliminar?',{ 
			aceptar: function(){
				$.post(app.url + 'usuario/eliminar/'+id, function(result){
					if (result.exito === true){
						$.noticia(result.msj,'success');
						$refrescarNotificaciones();
					}else{
						if ( result.exito === false ) {
							$.noticia(result.msj,'error');
						};				
					};
				},'json');
			}
		});
    });
    $modalCambiarPassword = (function(el){
		$('#id_usuario_cp').val($(el).attr('data-id-usuario'));
		$('#usuario_cp').val($(el).attr('data-usuario'));
		$('#correoE_cp').val($(el).attr('data-correo'));
		$('#ventanaCambiarContrasena').modal('show');
    });
    $cambiarStatus = (function(el){
    	params = $(el).attr("id").split('-');
		id_usuario = params[1];
		estado = $('span#estado-'+id_usuario).html();
		$('span#estado-'+id_usuario).removeClass().addClass("wait").html("");
		$.post(app.url + 'usuario/cambiar_estado',{ id_usuario : id_usuario, estado : estado },function(result){
			if(result.success === true){
				$('span#estado-'+id_usuario).removeClass("wait");
				if(result.estado == true){
					$.noticia(result.msj,'error');
					$('span#estado-'+id_usuario).addClass("label label-important state");
					$('span#estado-'+id_usuario).html("INACTIVO");
				}else{
					$.noticia(result.msj,'success');
					$('span#estado-'+id_usuario).addClass("label label-success state");
					$('span#estado-'+id_usuario).html("ACTIVO");						
				}
			}else{
				$.noticia(result.msj,'error');
			}
		},"json");
		return false;
    });
    $refrescarNotificaciones = (function(){
        var filas = '<?php echo $this->totalFilas; ?>';
        var rango = 0;
        if ( $('div#paginacion ul li.active a').length ) {
            if ( $('div#paginacion ul li.active a').html() !== '') {
                rango = ($('div#paginacion ul li.active a').html() - 1) * filas; 
            };
        }else{
            rango = 0;
        };
        $.post(app.url + 'usuario/paginacionUsu/'+rango, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
    });
    $paginacion = (function(el){
        var parametros = ($(el).attr('href'));
        $.post(parametros, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$todasFunciones = (function(){
		$('div#paginacion ul li a.btn-paginar').on('click', function(e){
	        e.preventDefault();
	        $paginacion(this);
	    });
	    $('table tbody tr a.btn-editar').on('click',function() {
	        $modalEdicion(this);
	    });
		$('table tbody tr a.btn-eliminar').on('click',function() {
	        $modalEliminar(this);
	    });
	    $('table tbody tr a.btn-cambiar-password').on('click',function() {
	        $modalCambiarPassword(this);
	    });
	    $('table tbody tr span.btn-status').on('click',function() {
	        $cambiarStatus(this);
	    });
	});
	$(document).ready(function() {
	    $todasFunciones();
	});
});
function finishAjax(id, response){
	$('#'+id).html(unescape(response));
	$('#'+id).fadeIn();
}
</script>
<style type="text/css"> .state:hover{ cursor:pointer; } </style>
<form id="busqueda" class="form-search well" action="#" method="post">
	<div class="input-append">
		<input type="text" class="search-query span4" placeholder="ingrese texto a buscar.." id="referencia" name="referencia" autofocus="autofocus"/>
		<button type="submit" id="btn-buscar" class="btn btn-success input-small"><i class="icon-search icon-white"></i></button>
	</div>
	<button id="ventanaNew" type="button" class="btn-agregar btn btn-primary opcion pull-right" title="Nuevo Usuario"><i class="icon-plus-sign icon-white"></i></button>
</form>
<form id="form_usuario" action="#"  method="post" onsubmit="return(false)" class="form-horizontal hide">
	<input type="hidden" id="modo" name="modo" value="0" />
	<input type="hidden" id="id_usuario" name="id_usuario" />
	<div class="row-fluid">
    	<div class="span6">
    		<legend class="lead">Datos Generales<span id="lbl-accion" class="badge pull-right status"></span></legend>
    		<div class="control-group" id="newNombre">
				<label class="control-label">Nombre</label>
				<div class="controls">	
			    	<input type="text" id="nombre" name="nombre" class="input-xlarge"/>
			    	<span class="help-inline" id="msjNombre_new"></span>
			     </div>
			</div>
			<div class="control-group" id="newCorreo">
				<label class="control-label">Correo Institucional</label>
				<div class="controls inline">	
			    	<input type="text" id="correo" name="correo" class="input-xlarge"/>
			    	<span class="help-inline" id="loading"><img src="<?php echo base_url(); ?>img/buscando.gif" alt="" /></span>
			    	<span class="help-inline" id="msjCorreo_new"></span>
			     </div>
			</div>
			<div class="control-group" id="newGrupo">
				<label class="control-label">Grupo</label>
				<div class="controls">
			    	<select id="id_rol" name="id_rol" class="input-xlarge">
			    		<?php $indice = 0; ?>
						<?php foreach ($roles as $key => $campo): ?>
							<?php if ( $indice == 0): ?>
								<option selected="selected">Seleccione</option>
								<option value="<?=$campo->id_rol?>" ><?=$campo->rol?></option>
							<?php else: ?>
								<option value="<?=$campo->id_rol?>"><?=$campo->rol?></option>
							<?php endif ?>
			        		<?php $indice++; ?>
			        	<?php endforeach ?>
			        </select>
			    	<span class="help-inline" id="msjGrupo_new"></span>
			     </div>
			</div>			
    	</div>
    	<div class="span6">
    		<legend class="lead">Datos de Acceso</legend>
			
			<div class="control-group" id="newUsuario">
				<label class="control-label">Usuario <br><small>min. 8 caracteres</small></label>
				<div class="controls">
			    	<input type="text" id="usuario" name="usuario" maxlength="10"/>
			    	<span class="help-inline" id="msjUsuario_new"></span>
			     </div>
			</div>
			<div class="control-group" id="contPass">
				<label class="control-label"></label>
				<div class="controls">
			    	<input type="text" class="fuerzaMsg fuerzaPass" name="fuerzaPass" value="Seguridad de la Contraseña" size="40" readonly="readonly">
			    </div> 
			</div>  
			<div class="control-group" id="newPassword">
			    <label class="control-label">Contrase&ntilde;a <br><small>min. 8 caracteres</small></label>
			    <div class="controls">
			    	<input type="password" id="password" name="password" maxlength="20"/> 
			    	<span class="help-inline" id="msjPass_new"></span>
			    </div>               
			</div>         
			<div class="control-group" id="newR_password">
			    <label class="control-label">Repetir Contrase&ntilde;a</label>
			    <div class="controls">
			    	<input type="password" id="confirm_password" name="confirm_password" maxlength="20"/>
			    	<span class="help-inline" id="msjPassR_new"></span>
			    </div> 
			</div>
    	</div>
	</div>
	<div class="form-actions">
		<button id="nuevoUsuario" type="submit" class="btn btn-primary pull-right" data-loading-text="Espere.." >Guardar</button>
		<span class="pull-right">&nbsp;</span>
		<button id="btn-cancelar-usuario" type="button" class="btn pull-right">Cancelar</button>
	</div>
</form>
<div id="contenedor">
	<table id="tabla-usuarios" class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	        	<th width="2%">ID</th>
	        	<th width="8%">USUARIO</th>
	            <th width="12%"></th>
	            <th width="10%">GRUPO</th>
	            <th width="8%">NOMBRE</th>
	            <th width="10%">CORREO</th>
	            <th width="5%">ESTADO</th>
	        </tr>
	    </thead>
	    <tbody>
		<?php if ($usuarios): ?>
			<?php foreach ($usuarios as $campo): ?>
		    	<tr>
		        	<td><?=$campo->id_usuario?></td>
		            <td><?=$campo->usuario?></td>
		            <td>
		            	<a data-id='<?=$campo->id_usuario?>' class="btn btn-danger btn-mini btn-eliminar pull-right opcion" title="Eliminar"><i class="icon-trash icon-white"></i></a>
		            	<span class="pull-right">&nbsp;</span>
			        	<a data-id-usuario='<?=$campo->id_usuario?>' data-usuario="<?=$campo->usuario?>" data-correo="<?=$campo->correo?>" class="btn btn-inverse btn-mini btn-cambiar-password pull-right opcion" title="Cambiar Contrase&ntilde;a"><i class="icon-lock icon-white cambiarContrasena"></i></a>
			        	<span class="pull-right">&nbsp;</span>
			        	<a data-id-usuario='<?=$campo->id_usuario?>' class="btn btn-info btn-mini btn-editar pull-right opcion" title="Editar"><i class="icon-edit icon-white"></i></a>	
		            </td>
		            <td><?=$campo->rol?></td>
		            <td><?=$campo->nombre?></td>
		            <td><?=$campo->correo?></td>
		            <td>
		            <?php
		                switch($campo->estado){
		                    case 'ACTIVO': ?><span id="estado-<?=$campo->id_usuario?>" class="label label-success btn-status">ACTIVO</span><?php
		                    	break;
		                    case 'INACTIVO': ?><span id="estado-<?=$campo->id_usuario?>" class="label label-important btn-status">INACTIVO</span><?php
		                    	break;
		                }
		            ?>                        
		            </td>
		        </tr> 
		    <?php endforeach ?>
		<?php else: ?>
				<tr>
	    			<td colspan="8">No se encontraron resultados</td>
	    		</tr>
		<?php endif ?>
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
</div>

<div id="ventanaNuevo" class="modal hide fade" style="width:984px;left:36%">
	<div class="modal-header lead">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<span>Nuevo Usuario</span>
	</div>
	<div class="modal-body">
		
    </div>
    <div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal" >Cancelar</a>
		<button id="nuevoUsuario" name="nuevoUsuario" class="btn btn-primary" type="button" data-loading-text="Espere..">Guardar</button>
	</div>
</div>

<div id="ventanaCambiarContrasena" class="modal hide fade">
	<div class="modal-header lead">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<span>Cambiar Contrase&ntilde;a</span>
	</div>
	<div class="modal-body">
        <form id="form_cambiar" action="#"  method="post" onsubmit="return(false)" class="form-horizontal">
        	<div class="control-group" id="newAviso">
				<label class="control-label"></label>						
				<div class="controls">	
			    	<span class="help-inline" id="msjAviso_new"></span>
			     </div>
			</div>   
            <div class="control-group">
                <input type="hidden" id="id_usuario_cp" name="id_usuario_cp" />
                <input type="hidden" id="usuario_cp" name="usuario_cp" />
                <input type="hidden" id="correoE_cp" name="correoE_cp" />
            	<label class="control-label">&nbsp;</label>
            	<div class="controls">
                	<input type="text" class="fuerzaMsg fuerzaPass" name="fuerzaPass" value="Seguridad de la Contraseña" size="40" readonly="readonly">
                </div>
            </div>  
            <div class="control-group" id="np_password">
                <label class="control-label">Nueva Contrase&ntilde;a</label>
                <div class="controls">
	                <input type="password" id="password_new" name="password_new" tabindex="8" />
	                <span class="help-inline" id="msjPass_np"></span>
                </div>
            </div>
            <div class="control-group" id="np_Rpassword">
                <label class="control-label">Repetir Contrase&ntilde;a</label>
                <div class="controls">
	                <input type="password" id="password_confirm" name="password_confirm" tabindex="9" />
	                <span class="help-inline" id="msjPassR_np"></span>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal" >Cancelar</a>
		<button id="nuevaPass" name="nuevaPass" class="btn btn-primary" type="button" data-loading-text="Espere..">Guardar</button>
	</div>
</div>

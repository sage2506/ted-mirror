<script type="text/javascript">
$(function(){
	$("#autocomplete").autocomplete({
		source: function(request, response) {
			$.post(app.url + 'dependencia/autocompletar',{ termino: $("#autocomplete").val() },
			function(result){
				if (result.dependencias.length == 0){
					result.dependencias = [{value:null,label:"Dependencia no existe."}];
					response(result.dependencias);
				}else{
					response(	
						$.map(result.dependencias, function(item){
							return { 
								label: item.dependencia,
								value: item.dependencia,
								id_dependencia : item.id_dependencia
							}
						})
					);
				}
			},'json'); 
		},
		selectFirst: false,
		minLength: 3,
		select: function(event, ui){
			$('#parent_id').val(ui.item.id_dependencia);
		}
	}).change(function(){
		if( $(this).val() !== $("id_dependencia").data('autocomplete') ){
			$("#id_dependencia").val('');
			$("id_dependencia").data('autocomplete','');
		}
		return false;
	});
	$('#ventanaNuevo').on('hidden', function () {
		$("#contenedorDependencia").removeClass("error");
		$("#contenedorPadre").removeClass("error");
		$('#msjError').html('');
		$('#id_dependencia').val('');
		$('#dependencia').val('');
		$('#autocomplete').val('');
		$('#autocomplete2').val('');
		$('#parent_id').val('');
	});
	$('#ventanaNew').click(function(){
		$('#titulo').html('Registro de Nuevo Dependencia');
		$('#ventanaNuevo').modal('show');
	});	
	$('#nuevaDependencia').click(function(){
		$('#nuevaDependencia').button('loading');
		$.post(app.url + 'dependencia/alta', $('#form_dependencia').serialize(), function(resultado){
			if (resultado.exito === true){
				$('#nuevaDependencia').button('reset');
				$('#ventanaNuevo').modal('hide');
				$.noticia(resultado.mensaje,'success');
                $refrescarNotificaciones();
			}else{
				if (resultado.exito === false) {
					$('#contenedorDependencia').addClass("error");
					$('#msjError').html(resultado.mensaje);
					$('#nuevaDependencia').button('reset');
				}
			}
		},'json');
		return false;
	});
	$('#btn-buscar').click(function(){
        $.post(app.url + 'dependencia/busqueda', $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
	$modalEdicion = (function(el){
        var dependencia = $(el).attr('data-dependencia');
        var padre = $(el).attr('data-padre');
		$('#titulo').html('Edición de la Dependencia');
		$('#modo').val(1);
		$('#id_dependencia_ss').val(dependencia);
		$.post(app.url + 'dependencia/edicion', {id_dependencia: dependencia, parent_id: padre},function(result){
			var row = result.dependencia;
			$('#dependencia').val(row.dependencia);
			$('#autocomplete').val(row.nombre_padre);
			$('#parent_id').val(row.parent_id);
			$('#id_dependencia_ss').val(row.id_dependencia);
		},'json');
        $('#ventanaNuevo').modal('show');
        return false;
    });
    $refrescarNotificaciones = (function(){
        var filas = '<?php echo $this->totalFilas; ?>';
        var rango = 0;
        if ( $('div#paginacion ul li.active a').length ) {
            if ( $('div#paginacion ul li.active a').html() !== '') {
                rango = ($('div#paginacion ul li.active a').html() - 1) * filas; 
            };
        }else{
            rango = 0;
        };
        $.post(app.url + 'dependencia/paginacionDep/'+rango, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
    });
    $paginacion = (function(el){
        var parametros = ($(el).attr('href'));
        $.post(parametros, $('#busqueda').serialize(), function(data){
            $('#contenedor').html(data);
            $todasFunciones();
        });
        return false;
    });
    $todasFunciones = (function(){
        $('div#paginacion ul li a.btn-paginar').on('click', function(e){
            e.preventDefault();
            $paginacion(this);
        });
        $('table tbody tr a.btnEdicion').on('click',function(e) {
			e.preventDefault();
		    $modalEdicion(this);
		    return false;
		});
    });	
	$(document).ready(function() {
        $todasFunciones();
    });
});
</script>
<form id="busqueda" name="busqueda" class="form-search well" action="#" method="post">
	<div class="input-append">
		<input type="text" class="search-query span4" placeholder="ingrese texto a buscar.." id="referencia" name="referencia" autofocus="autofocus"/>
		<button type="submit" id="btn-buscar" class="btn btn-success input-small"><i class="icon-search icon-white"></i></button>
	</div>
	<button id="ventanaNew" type="button" class="btn btn-primary pull-right opcion" title="Nueva Dependencia" data-placement="left"><i class="icon-plus-sign icon-white"></i> </button>
</form>
<div id="contenedor">
	<table class="table table-striped table-hover table-condensed">
	    <thead>
			<tr>
				<th>NOMBRE</th>
				<th style="width:10px"></th>
			</tr>
	    </thead>
	    <tbody>
	    	<?php if ($dependencias): ?>
	    		<?php foreach ($dependencias as $campo): ?>
					<tr>
				        <td align="left"><?=$campo->dependencia?></td>
				        <td>
				        	<a class="btn btn-mini btn-info btnEdicion opcion" title="Editar" data-toggle="modal" href="#ventanaNuevo" rel="tooltip" data-dependencia='<?=$campo->id_dependencia?>' data-padre="<?=$campo->parent_id?>"><i class="icon-white icon-edit"></i></a>
				        </td>            
				    </tr> 
				<?php endforeach ?>
	    	<?php else: ?>
	    		<tr>
	    			<td colspan="2">No se encontraron resultados</td>
	    		</tr>
	    	<?php endif ?>
	    </tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
</div>
<!--Ventana Nuevo-->
<div id="ventanaNuevo" class="modal hide fade" style="width:591px;left:48%">
	<div class="modal-header lead">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<span id="titulo"></span>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" id="form_dependencia" action="#"  method="post" >
			<input type="hidden" id="modo" name="modo" value="0" />
    		<input type="hidden" id="id_dependencia_ss" name="id_dependencia_ss" />
	        <fieldset>
				<div class="control-group" id="contenedorDependencia" >
					<label class="control-label" for="input01">Nombre Dependencia</label>
					<div class="controls">
						<input type="text" id="dependencia" name="dependencia" style="width: 350px;"/>
						<span class="help-inline" id="msjError"></span>
					</div>
				</div>
				<div class="control-group" id="contenedorPadre" >
					<label class="control-label" for="input01">Dependencia de la que depende</label>
					<div class="controls">
						<input id="autocomplete" name="autocomplete" type="text" style="width: 350px;"/>
						<input type="hidden" id="parent_id" name="parent_id" />
						<span class="help-inline" id="msjError"></span>
					</div>
				</div>
	        </fieldset>
	    </form>
	</div>
	<div class="modal-footer">
		<button class="btn" >Cancelar</button>
		<button id="nuevaDependencia" name="nuevaDependencia" class="btn btn-primary" type="button" data-loading-text="Espere..">Guardar</button>
	</div>								
</div>
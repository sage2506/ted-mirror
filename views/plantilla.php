<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$this->config->item('nombreAplicacion')?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsabilidades Administrativas">
    <meta name="author" content="Antonio Yee yee.antonio@gmail.com">
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le styles -->
    <base href="<?=base_url()?>" target="_self" /> <!-- Url de aplicacion  -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; }
    </style>
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="css/custom-theme/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src="js/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js"></script>
		<script type="text/javascript">
		function validar(f){
				var ext = ['pdf'];
				var v = f.value.split('.').pop().toLowerCase();
				if (v == ext[0]) {
						$('#contArchivo').removeClass("error");
				$('#msjArchivo').html('');
				}else{
						$('#image1').val('');
						$('#contArchivo').addClass("error");
				$('#msjArchivo').html('<span class="help-inline">Solo se permite archivo .PDF</span>');
				};
		}
		</script>
</head>
<body>
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
      <div class="cargando" id="cargando">
        <div class="msg progress progress-warning progress-striped active">
          <div class="bar" style="width: 100%;">Cargando</div>
        </div>
      </div>
      <div class="container-fluid">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="panel/historial"><?=$this->config->item('nombreAplicacion')?></a>
        <div class="nav-collapse collapse">
          <ul class="nav">
            <li id="li-principal"><a href="panel/historial">Historial</a></li>
            <li id="li-sanciones"><a href="panel/sancionar">Sancionar</a></li>
            <li id="li-constancias"><a href="panel/constancias">Constancias</a></li>
          </ul>
          <ul class="nav pull-right">
            <li><a><?php echo $this->session->userdata('nombre'); ?></a></li>
            <?php if ($this->session->userdata('id_rol')==1): ?>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Seguridad <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="dependencia/listado"><i class="icon-globe"></i> Dependencias</a></li>
                    <li><a href="usuario/listado"><i class="icon-user"></i> Usuarios</a></li>
                    <li id="li-rol"><a href="rol/listado"><i class="icon-briefcase"></i> Roles</a></li>
                    <li id="li-modulo"><a href="modulo/listado"><i class="icon-wrench"></i> Módulos</a></li>
                  </ul>
                </li>
            <?php endif ?>
            <li class="divider-vertical"></li>
            <li><a href="sesion/logout">Cerrar Sesión</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <ul class="breadcrumb">
          <a href=""><i class="icon-home"></i></a>
        </ul>
      </div>
    </div>
  </div>
	<div class="container-fluid">
		<!-- vaciar la informacion en el contenedor-->
		<?php echo $plantilla; ?>
    <div id="notificacion" class='notifications top-right'></div>



		<!-- Modal para confirmacion de borrado de sancion-->
    <div id="confirmacion" class="modal hide fade">
      <div class="modal-header lead">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        Confirmación
      </div>
      <div class="modal-body">
        <p><i class="icon-warning-sign"></i><span></span></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-small" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-small btn-primary" data-loading-text="Espere.." data-complete-text="Eliminar">Eliminar</button>
      </div>
    </div>


		<!-- TODO: aqui modal para subir la ejecución -->
		<div id="ejecucion" class="modal hide fade">
      <div class="modal-header lead">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        Ejecución
      </div>
			<form class="form-horizontal" id="form_ejecucion" name="form_nuevo" method="post" action="#" onsubmit="return(false)" enctype="multipart/form-data">
      <div class="modal-body">
				<input type="hidden" id="id_sancionado_ejecutar" name="id_sancionado_ejecutar" value="NULL" >
				<div class="control-group" id="contArchivo" >
					<label class="control-label">Archivo</label>
					<div class="controls">
						<input type="file" class="span12" id="image1" name="image1" onchange="validar(this)" >
						<div id="msjArchivo"></div>
					</div>
				</div>
        <p><i></i><span></span></p>
      </div>
			</form>
      <div class="modal-footer">
        <button type="button" class="btn btn-small" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-small btn-primary" data-loading-text="Espere.." data-complete-text="Eliminar">Subir</button>
      </div>
    </div>





		<footer>
      <p class="fl" style="background: url(<?=base_url()?>img/escudo_mini.png) no-repeat;width:35px;height:51px; float:left;padding-right:7px"></p>
			<address class="fl" style="margin-left:10px;">
				<strong>H.Ayuntamiento de Culiacán</strong><br>
				Coordinación General de Desarrollo Tecnológico<br>
				<abbr title="Teléfono">Tel:</abbr>758-01-01 <abbr title="Extensión">Ext:</abbr>1271
			</address>
	  </footer>
	</div>
</body>
</html>

<script type="text/javascript">

function validar(f){
    var ext = ['pdf'];
    var v = f.value.split('.').pop().toLowerCase();
    if (v == ext[0]) {
        $('#contArchivo').removeClass("error");
		$('#msjArchivo').html('');
    }else{
        $('#image1').val('');
        $('#contArchivo').addClass("error");
		$('#msjArchivo').html('<span class="help-inline">Solo se permite archivo .PDF</span>');
    };
}
function ajusteFormulario(e){
  console.log($('#sancion').val());
  $("#camposExtra").html("<div id='CREADO'>Hola mundo</div>");
  $.post(app.url+'panel/sancionExtras',{id_tipo_sancion : $('#sancion').val()},function(response){
    $("#camposExtra").html(response);
  });
}
$(function() {
	$limpiarTodo = (function(){
		$("#contNombre").removeClass("error");
		$('#nombre').val('');
		$('#msjNombre').html('');
		$("#contPaterno").removeClass("error");
		$('#pat').val('');
		$('#msjPaterno').html('')
		$("#contMaterno").removeClass("error");
		$('#mat').val('');
		$('#msjMaterno').html('')
		$("#contDireccion").removeClass("error");
		$('#direccion').val('');
		$('#msjDireccion').html('')
		$("#contColonia").removeClass("error");
		$('#col').val('');
		$('#msjColonia').html('')
		$("#contCP").removeClass("error");
		$('#cp').val('');
		$('#msjCP').html('')
		$("#contTel").removeClass("error");
		$('#tel').val('');
		$('#msjTel').html('')
		$("#contCel").removeClass("error");
		$('#cel').val('');
		$('#msjCel').html('')
		$("#contCorreo").removeClass("error");
		$('#correo').val('');
		$('#msjCorreo').html('');
		$("#contDepen").removeClass("error");
		$('#autocomplete').val('');
		$('#id_dependencia').val('');
		$('#msjDepen').html('');
		$("#contSancion").removeClass("error");
		$('#sancion> option[value="0"]').attr('selected', 'selected');
		$('#msjSancion').html('');
		$("#contCargo").removeClass("error");
		$('#cargo').val('');
		$('#msjCargo').html('');
		$('#msjError').html('');
		$('#select-funcionario').val('');
		$('#id_funcionario').val('');
		$('#contArchivo').removeClass("error");
		$('#msjArchivo').html('');
		$('#image1').val('');
		$('#contExp').removeClass("error");
		$('#msjExp').html('');
		$('#numero-expediente').val('');
	});
	$('.opcion').tooltip();
	$('#li-sanciones').addClass('active');
	if ( ! $('ul.breadcrumb li.active').html() ) {
		$('ul.breadcrumb').append('<li class="active">Sancionar</li>');
	};
	$("#select-funcionario").autocomplete({
		source: function(request, response) {
			$.post(app.url + 'panel/au_funcionarios',{ termino: $("#select-funcionario").val() },
			function(result){
				if (result.funcionarios.length == 0){
					result.funcionarios = [{value:null,label:"Funcionario no existe."}];
					response(result.funcionarios);
				}else{
					response(
						$.map(result.funcionarios, function(item){
							return {
								label: item.nombre + ' ' + item.ap_paterno + ' ' + item.ap_materno,
								value: item.nombre + ' ' + item.ap_paterno + ' ' + item.ap_materno,
								id_funcionario : item.id_funcionario
							}
						})
					);
				}
			},'json');
		},
		selectFirst: false,
		minLength: 3,
		select: function(event, ui){
			$('#id_funcionario').val(ui.item.id_funcionario);
			$.post(app.url + 'panel/seleccion',{ id_funcionario : $('#id_funcionario').val() },function(resultado){
				$.each(resultado, function(index,value){
					$('#resultadosBusqueda').modal('hide');
					$('#id_funcionario').val(value.id_funcionario);
					$('#nombre').val(value.nombre);
					$('#pat').val(value.ap_paterno);
					$('#mat').val(value.ap_materno);
					$('#direccion').html(value.direccion);
					$('#col').val(value.colonia);
					$('#cp').val(value.cp);
					$('#tel').val(value.telefonos);
					$('#correo').val(value.correo);
					$('#autocomplete').val('').focus();
					$('#id_dependencia').val('');
	    		});
			},'json');
		}
	}).change(function(){
		if( $(this).val() !== $("id_funcionario").data('select-funcionario') ){
			$("#id_funcionario").val('');
			$limpiarTodo();
			$("id_funcionario").data('select-funcionario','');
		}
		return false;
	});
	$("#autocomplete").autocomplete({
		source: function(request, response) {
			$.post(app.url + 'dependencia/autocompletar',{termino: $("#autocomplete").val()},
			function(result){
				if (result.dependencias.length == 0){
					result.dependencias = [{value:null,label:"Dependencia no existe."}];
					response(result.dependencias);
				}else{
					response(
						$.map(result.dependencias, function(item){
							return {
								label: item.dependencia,
								value: item.dependencia,
								id_dependencia : item.id_dependencia
							}
						})
					);
				}
			},'json');
		},
		selectFirst: false,
		minLength: 3,
		select: function(event, ui){
			if ( $('#nombre').val() !== '' ) {
				$('#cargo').focus();
			}else{
				$('#nombre').focus();
			};
			$('#id_dependencia').val(ui.item.id_dependencia);
		}
	}).change(function(){
		if( $(this).val() !== $("id_dependencia").data('autocomplete') ){
			$("#id_dependencia").val('');
			$("id_dependencia").data('autocomplete','');
		}
		return false;
	});
	$('#btn-cancelar').click(function(){
		$limpiarTodo();
		return false;
	});
	$('#agregarFunc').click(function(){
		$('#agregarFunc').button('loading');
		var sancion = new FormData(document.getElementById("form_nuevo"));
		$.ajax({
			url: 	app.url + 'panel/sancionarFuncionario',
			type: 	'POST',
			cache:	false,
			data: sancion,
			processData: false,
			contentType: false,
			success: function(resultado){
				if ( resultado.exito ){
					window.document.location = app.url + 'panel/historial/' + resultado.sancionado;
				}else{
					$('#agregarFunc').button('reset');
					if ( $('#sancion').val() === '0' ) {
						$('#contSancion').addClass("error");
						$('#msjSancion').html('<span class="help-inline">El campo Sanción es obligatorio.</span>');
					};
					if ( $('#image1').val() === '' ) {
						$('#contArchivo').addClass("error");
						$('#msjArchivo').html('<span class="help-inline">El campo Archivo es obligatorio.</span>');
					};
					if ( resultado.validacion ) {
						$.noticia(resultado.msj_validacion,'error');
					}else{
						first = true;
						$.each(resultado.mensaje, function(campo, aviso){
							if (first){
								$('div .error').removeClass('error');
								error = new Object({ campo: campo, mensaje: aviso });
								first = false;
							}
							$('#'+campo).parents('.control-group').addClass('error');
						});
						$.noticia(error.mensaje, 'danger');
						$('#'+error.campo).focus();
					};
				};
			}
		});
	});
});
</script>
<form class="form-search well" action="#" method="post" onsubmit="return(false)" >
	<div class="controls">
		<div class="input-prepend" style="margin-left:-10px;">
          <span class="add-on"><i class="icon-search"></i></span>
          <input id="select-funcionario" name="select-funcionario" type="text" class="input-xxlarge conv-mayu" placeholder="Buscar Funcionario" autofocus="autofocus">
        </div>
	</div>
</form>
<form class="form-horizontal" id="form_nuevo" name="form_nuevo" method="post" action="#" onsubmit="return(false)" enctype="multipart/form-data">
	<input type="hidden" id="id_funcionario" name="id_funcionario" value="<?=$san_id_funcionario?>">
	<div class="row-fluid">
	    <div class="span4">
	    	<legend>Información del Funcionario</legend>
	    	<div class="control-group" id="contDepen">
				<div class="controls">
					<div class="input-prepend">
		              <span class="add-on"><i class="icon-search"></i></span>
		              <input id="autocomplete" name="autocomplete" class="conv-mayu" type="text" placeholder="Buscar Dependencia" style="width:100%;">
		            </div>
					<input type="hidden" id="id_dependencia" name="id_dependencia" />
					<span class="help-inline" id="msjDepen"></span>
				</div>
			</div>
	    	<div class="control-group" id="contNombre" >
				<label class="control-label">Nombre</label>
				<div class="controls">
					<input type="text" class="span12 conv-mayu" id="nombre" name="nombre" value="<?=$san_nombre?>">
					<div id="msjNombre"></div>
				</div>
			</div>
			<div class="control-group" id="contPaterno" >
				<label class="control-label">Apellido Paterno</label>
				<div class="controls">
					<input type="text" class="span12 conv-mayu" id="pat" name="pat" value="<?=$san_ap_paterno?>">
					<div id="msjPaterno"></div>
				</div>
			</div>
			<div class="control-group" id="contMaterno" >
				<label class="control-label">Apellido Materno</label>
				<div class="controls">
					<input type="text" class="span12 conv-mayu" id="mat" name="mat" value="<?=$san_ap_materno?>">
					<div id="msjMaterno"></div>
				</div>
			</div>
			<div class="control-group" id="contCargo" >
				<label class="control-label">Cargo</label>
				<div class="controls">
					<input type="text" class="span12 conv-mayu" id="cargo" name="cargo">
					<div id="msjCargo"></div>
				</div>
			</div>
			<div class="control-group" id="contCorreo" >
				<label class="control-label">Correo Eléctronico</label>
				<div class="controls">
					<input type="text" class="span12" id="correo" name="correo" value="<?=$san_correo?>">
					<div id="msjCorreo"></div>
				</div>
			</div>
	    </div>
	    <div class="span4">
	    	<legend>Contacto</legend>
	    	<div class="control-group" id="contDireccion" >
				<label class="control-label">Dirección</label>
				<div class="controls">
					<textarea class="span12 conv-mayu" id="direccion" name="direccion" rows="3" ><?=$san_direccion?></textarea>
					<span class="help-block">Máx. 255 caracteres.</span>
					<div id="msjDireccion"></div>
				</div>
			</div>
			<div class="control-group" id="contColonia" >
				<label class="control-label">Colonia</label>
				<div class="controls">
					<input type="text" class="span12 conv-mayu" id="col" name="col" value="<?=$san_colonia?>">
					<div id="msjColonia"></div>
				</div>
			</div>
			<div class="control-group" id="contCP" >
				<label class="control-label" for="input01">Código Postal</label>
				<div class="controls">
					<input type="text" class="span12 conv-mayu" id="cp" name="cp" value="<?=$san_cp?>">
					<div id="msjCP"></div>
				</div>
			</div>
			<div class="control-group" id="contTel" >
				<label class="control-label">Teléfonos</label>
				<div class="controls">
					<textarea class="span12 conv-mayu" id="tel" name="tel" rows="3" ><?=$san_telefonos?></textarea>
					<span class="help-block">Máx. 100 caracteres.</span>
					<div id="msjTel"></div>
				</div>
			</div>
	    </div>
	    <div class="span4">
	    	<legend>Sanción</legend>
	    	<div class="control-group" id="contExp" >
				<label class="control-label">Número de Expediente</label>
				<div class="controls">
					<input type="text" class="span12 conv-mayu" id="numero-expediente" name="numero-expediente" >
					<div id="msjExp"></div>
				</div>
			</div>
			<div class="control-group" id="contSancion" >
				<label class="control-label">Sanción</label>
				<div class="controls">
					<select name="sancion" id="sancion" class="span12" onchange="ajusteFormulario(this)">
						<option selected="selected" value="0">Seleccione</option>
		                <?php foreach ($catSanciones as $campo):?>
							<option value="<?=$campo->id_sancion?>"><?=$campo->nombre?></option>
		                <?php endforeach; ?>
		            </select>
					<span class="help-inline" id="msjSancion"></span>
				</div>
			</div>
      <div class="controls-group" id="camposExtra"></div>
			<div class="control-group" id="contArchivo" >
				<label class="control-label">Archivo</label>
				<div class="controls">
					<input type="file" class="span12" id="image1" name="image1" onchange="validar(this)" >
					<div id="msjArchivo"></div>
				</div>
			</div>
	    	<div class="control-group" id="contExtracto	" >
				<label class="control-label">Observaciones</label>
				<div class="controls">
					<textarea class="span12 conv-mayu" id="extracto" name="extracto" rows="6"></textarea>
					<div id="msjExtracto"></div>
				</div>
			</div>
	    </div>
	</div>
	<div class="form-actions">
		<button type="submit" id="agregarFunc" name="agregarFunc" class="btn btn-primary pull-right" data-loading-text="Espere..">Guardar</button>
		<span class="pull-right">&nbsp;</span>
		<input type="reset" id="btn-cancelar" class="btn pull-right" value="Cancelar" />
	</div>
</form>
<div id="resultadosBusqueda" class="modal hide fade" tabindex="-1" role="dialog" style="width:800px;left:43%" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header lead">
		<a class="close" data-dismiss="modal" >&times;</a>
		<h3 id="myModalLabel">Resultados</h3>
	</div>
	<div class="modal-body">
		<div id="listadoResultados"></div>
	</div>
</div>

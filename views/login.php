<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$this->config->item('nombreAplicacion')?> | Iniciar Sesión</title>
<meta name="description" content="Responsabilidades Administrativas">
<meta name="author" content="Antonio Yee yee.antonio@gmail.com">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate"/> 
<meta http-equiv="Pragma" content="must-revalidate"/> 
<meta http-equiv="Expires" content="0"/> 
<meta name="robots" content="noindex, none" />
<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<base href="<?=base_url()?>" target="_self" /> <!-- Url de aplicacion  -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/usuarios.login.css">
<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="ico/favicon.ico">
<!-- <link type="text/css" href="css/usuarios.login.css" rel="stylesheet" />-->
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/tipTip.js"></script>
<script type="text/javascript"> 
function activar() {
    $('#cargando').css({'display' : 'none'});
    $('#usuario, #password, #acceso').removeAttr('disabled');
    $('#usuario, #password').removeClass("caja");
    $('#acceso').removeClass("desactivar");
}
function desactivar() {
    $('#cargando').css({'display' : 'block'});
    $('#usuario, #password, #acceso').attr("disabled", true);
    $('#usuario, #password').addClass("caja");
    $('#acceso').addClass("desactivar");
}
$(function(){
    $('#acceso').click(function(event){
        event.preventDefault();
        desactivar();
        if ($('#usuario').val().length == 0){
            setTimeout(function() {
                activar();
                $('#usuario').attr('title','El campo usuario es obligatorio').addClass('error').tipTip({delay:100,defaultPosition:'right',activation:'focus'}).focus();
            },700);
            return false;
        } else if ($('#password').val().length == 0){
            setTimeout(function() {
                activar();
                $('#password').attr('title','El campo contraseña es obligatorio').addClass('error').tipTip({delay:100,defaultPosition:'right',activation:'focus'}).focus();
            },700);
            return false;
        }else{
            $.post('<?=base_url()?>sesion/jx_auth', { usuario: $('#usuario').val(), password: $('#password').val() }, 
            function(result) {
                if(result.exito === true) {
                    setTimeout(function() {
                        window.document.location = result.redirect;
                    },700);
                    return false;
                } else {  
                    setTimeout(function() {
                        activar();
                        $('#'+result.aviso).attr('title',result.msj).addClass('error').tipTip({delay:100,defaultPosition:'right',activation:'focus'}).focus();
                    },700);
                    return false;
                }
            },'json');
            return false;   
        }
    });
});
</script>
<style>
.caja{ background-color: #eee !important; }
.desactivar, .desactivar:hover {
    background: #ECECEC;
    color: #a9a9a9;
    border: 1px solid #ccc;
}
</style>
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="brand" href="<?=base_url()?>"><?=$this->config->item('nombreAplicacion')?></a>
            </div>
        </div>
    </div>
    <div id="content" class="well">
        <div class="logo"></div>
        <div class="separador"></div>
        <form style="width:225px;float:right;" id="form-login" name="form-login" action="" method="POST" onsubmit="return(false)" class="form-stacked">
            <fieldset style="margin:0 auto;">
                <div class="clearfix">
                    <label for="usuario">Usuario</label>
                    <div class="input">
                        <input type="text" name="usuario" id="usuario" value="" tabindex="10" autocomplete="off" style="font-size:24px !important;padding:7px !important;"/>
                    </div>
                </div>
                <div class="clearfix">
                    <label for="clave">Contraseña</label>
                        <input type="password" name="password" id="password" value="" tabindex="20" style="font-size:24px !important;padding:7px !important;"/>
                    <div class="input">
                    </div>
                </div>
                <span id="cargando" class="cargando">Verificando..</span>
                <button type="submit" class="btn btn-primary" style="float:right;margin-top:3px;" name="acceso" id="acceso" tabindex="30">Iniciar Sesión</button>
            </fieldset>
        </form>
    </div>
    <footer class="footer" style="height:57px">
        <address class="fl" style="margin-left:10px;">
            <strong>H.Ayuntamiento de Culiacán</strong><br>
            Coordinación General de Desarrollo Tecnológico<br>
            <abbr title="Teléfono">Tel:</abbr>758-01-01 <abbr title="Extensión">Ext:</abbr>1271
        </address>
    </footer>    
</body>
</html>
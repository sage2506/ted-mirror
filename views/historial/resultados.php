<script type="text/javascript">
$(function() {
	$(".opcion").tooltip();
	$('#ventanaHistorial').on('hidden', function () {
		$('#nombre').html('');
		$('#dependencia').html('');
		$('#cargo').html('');
		$('#direccion').html('');
		$('#telefonos').html('');
		$('#correo').html('');
		$('#infoSanciones').html('');
	});	
});
</script>
<table id="tabla-listado" class="table table-striped table-hover table-condensed">
	<thead>
		<tr>
			<th width="65%">NOMBRE</th>
			<th width="20%">SANCIONES</th>
			<th width="15%"></th>
		</tr>
	</thead>
	<tbody>
		<?php if ($sancionados): ?>
			<?php foreach ($sancionados as $campo): ?>
				<tr>
					<td><?=$campo->nombre?> <?=$campo->ap_paterno?> <?=$campo->ap_materno?></td>
					<td><?=$campo->totSanciones?></td>
					<td>
						<a href="panel/sancionar/<?=$campo->id_funcionario?>" class="btn btn-mini btn-danger opcion pull-right" title="Sancionar"><i class="icon-thumbs-down icon-white"></i></a>
						<span class="pull-right">&nbsp;</span>
						<a data-id-funcionario="<?=$campo->id_funcionario?>" class="btn-editar btn btn-mini btn-success opcion pull-right" title="Editar"><i class="icon-pencil icon-white"></i></a>
						<span class="pull-right">&nbsp;</span>
						<a class="btn btn-mini btn-info btn-ver-historial opcion pull-right" data-id-funcionario="<?=$campo->id_funcionario?>" title="Historial de Sanciones"><i class="icon-list-alt icon-white"></i></a>
					</td>
				</tr>
			<?php endforeach ?>
		<?php else: ?>
			<tr>
				<td colspan="3" >No se encontraron resultados.</td>
			</tr>
		<?php endif ?>
	</tbody>
</table>
<?=$this->pagination->create_links()?>
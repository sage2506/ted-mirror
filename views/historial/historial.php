<script type="text/javascript">
$(function() {
	$('#li-principal').addClass('active');
	if ( ! $('ul.breadcrumb li.active').html() ) {
		$('ul.breadcrumb').append('<li class="active">Historial</li>');
	};
	$mostrarHistorial = (function(){
		$('#detalle-historial').slideDown();
	    $('#tabla-listado').slideUp();
	    $('div#paginacion').css({'display' : 'none'});
	});
	$ocultarHistorial = (function(){
		$('#detalle-historial').slideUp();
	    $('#tabla-listado').slideDown();
	    $('div#paginacion').css({'display' : 'block'});
	    $.post(app.url + 'panel/paginadoHistorial', function(data){
            $('#vaciarResultados').html(data);
        });
	});
	$('#btn-esconder').click(function(){
		$ocultarHistorial();
		$borrarDetalleHistorial();
	});
	$borrarDetalleHistorial = (function(){
		$("#id_funcionario").val('');
		$('#nombre').html('');
		$('#direccion').html('');
		$('#telefonos').html('');
		$('#correo').html('');
		$('#dep-ultima').html('');
		$('#car-ultimo').html('');
		$('#infoSanciones').html('');
	});
	$("#busqueda").autocomplete({
		source: function(request, response) {
			$.post(app.url + 'panel/au_funcionarios',{ termino: $("#busqueda").val() },
			function(result){
				if (result.funcionarios.length == 0){
					result.funcionarios = [{value:null,label:"Funcionario no existe."}];
					response(result.funcionarios);
				}else{
					response(
						$.map(result.funcionarios, function(item){
							return {
								label: item.nombre + ' ' + item.ap_paterno + ' ' + item.ap_materno,
								value: item.nombre + ' ' + item.ap_paterno + ' ' + item.ap_materno,
								id_funcionario : item.id_funcionario
							}
						})
					);
				}
			},'json');
		},
		selectFirst: false,
		minLength: 3,
		select: function(event, ui){
			$('#id_funcionario').val(ui.item.id_funcionario);
			if ( ui.item.id_funcionario ) {
				$.post(app.url + 'panel/historialFuncionario',{ id_funcionario : ui.item.id_funcionario },function(resultado){
					$.each(resultado.infoDetalle, function(index,value){
						$('#nombre').html(value.nombre + ' ' + value.ap_paterno + ' ' + value.ap_materno);
						if ( value.direccion ) {
							$('#direccion').html(value.direccion + ' ' + value.colonia + ' ' + value.cp);
						};
						$('#telefonos').html(value.telefonos);
						if ( value.correo ) {
							$('#correo').html('<a href="mailto:' + value.correo + '" target="_blank">' + value.correo + '</a>');
						};
						$('#dep-ultima').html(resultado.dependenciaUltima);
						$('#car-ultimo').html(resultado.cargoUltimo);
		    		});
		    		$('#infoSanciones').html(resultado.historialSanciones);
		    		$('#btn-sancionar-historial').attr( 'href' , 'panel/sancionar/'+ui.item.id_funcionario );
		    		$mostrarHistorial();
				},'json');
			};
		}
	}).change(function(){
		if( $(this).val() !== $("id_funcionario").data('busqueda') ){
			$("id_funcionario").data('busqueda','');
		}
		return false;
	});
	$('#buscar').click(function(){
		$.post(app.url + 'panel/busquedas',{ busqueda : $('#busqueda').val() },function(data){
			$ocultarHistorial();
			$borrarDetalleHistorial();
			$('#vaciarResultados').html(data);
		});
	});
	$verHistorialFuncionario = (function(el){
		$.post(app.url + 'panel/historialFuncionario',{ id_funcionario : $(el).attr('data-id-funcionario') },function(resultado){
			$.each(resultado.infoDetalle, function(index,value){
				$('#nombre').html(value.nombre + ' ' + value.ap_paterno + ' ' + value.ap_materno);
				if ( value.direccion ) {
					$('#direccion').html(value.direccion + ' ' + value.colonia + ' ' + value.cp);
				};
				$('#telefonos').html(value.telefonos);
				if ( value.correo ) {
					$('#correo').html('<a href="mailto:' + value.correo + '" target="_blank">' + value.correo + '</a>');
				};
				$('#dep-ultima').html(resultado.dependenciaUltima);
				$('#car-ultimo').html(resultado.cargoUltimo);
    		});
    		$('#infoSanciones').html(resultado.historialSanciones);
    		$('#btn-sancionar-historial').attr( 'href' , 'panel/sancionar/'+$(el).attr('data-id-funcionario') );
    		$mostrarHistorial();
		},'json');
		return false;
	});
	$paginacion = (function(el){
        var parametros = ($(el).attr('href'));
        $.post(parametros,$('#buscador').serialize(), function(data){
            $('#vaciarResultados').html(data);
        });
        return false;
    });
    $('div#vaciarResultados').on('click', 'table#tabla-listado tbody tr a.btn-ver-historial',function() {
        $verHistorialFuncionario(this);
    });
    $('div#vaciarResultados').on('click', 'div#paginacion ul li a.btn-paginar',function(e){
        e.preventDefault();
        $paginacion(this);
    });
    $('div#infoSanciones').on('click', 'table#tbl tbody tr',function(){
        el = $(this);
        if (el.find('td').css('white-space') == 'nowrap'){
            el.find('td').css('white-space','normal');
        }else{
            el.find('td').css('white-space','nowrap');
        }
    });
    $('div#vaciarResultados').on('click', 'table#tabla-listado tbody tr a.btn-editar',function(e){
    	$.post(app.url + 'panel/seleccion',{ id_funcionario : $(this).attr('data-id-funcionario') },function(resultado){
    		var persona = resultado.persona;
    		$('#id_funcionario_edicion').val(persona.id_funcionario);
    		$('#nombre_edicion').val(persona.nombre);
    		$('#pat_edicion').val(persona.ap_paterno);
    		$('#mat_edicion').val(persona.ap_materno);
    		$('#direccion_edicion').val(persona.direccion);
    		$('#col_edicion').val(persona.colonia);
    		$('#cp_edicion').val(persona.cp);
    		$('#tel_edicion').val(persona.telefonos);
    		$('#correo_edicion').val(persona.correo);
			$('#modal-edicion').modal('show');
		},'json');
		return false;
    });
		// aqui se define que se abre el modal con el borrar sancion
    $('div#infoSanciones').on('click', 'table#tbl tbody tr a.btn-eliminar-sancion',function(e){
    	var id_sancionado = $(this).attr('data-id-sancion');
    	var id_funcionario = $(this).attr('data-id-funcionario')
		$.confirmar('¿Seguro que desea eliminar la sanción?',{
			aceptar: function(){
				$.post(app.url + 'panel/eliminar_sancion',{ id_sancionado : id_sancionado }, function(resultado){
					if (resultado.exito === true){
						$.noticia(resultado.mensaje,'success');
						$.post(app.url + 'panel/historialFuncionario',{ id_funcionario : id_funcionario },function(resultado){
							$.each(resultado.infoDetalle, function(index,value){
								$('#nombre').html(value.nombre + ' ' + value.ap_paterno + ' ' + value.ap_materno);
								if ( value.direccion ) {
									$('#direccion').html(value.direccion + ' ' + value.colonia + ' ' + value.cp);
								};
								$('#telefonos').html(value.telefonos);
								if ( value.correo ) {
									$('#correo').html('<a href="mailto:' + value.correo + '" target="_blank">' + value.correo + '</a>');
								};
								$('#dep-ultima').html(resultado.dependenciaUltima);
								$('#car-ultimo').html(resultado.cargoUltimo);
				    		});
				    		$('#infoSanciones').html(resultado.historialSanciones);
				    		$('#btn-sancionar-historial').attr( 'href' , 'panel/sancionar/'+$(el).attr('data-id-funcionario') );
				    		$mostrarHistorial();
						},'json');
					}else{
						if ( resultado.exito === false ) {
							$.noticia(resultado.mensaje,'error');
						};
					};
				},'json');
			}
		});
    });

		$('div#infoSanciones').on('click', 'table#tbl tbody tr a.btn-agregar-ejecución',function(e){
			var id_sancionado = $(this).attr('data-id-sancion');
			var id_funcionario = $(this).attr('data-id-funcionario')
			$('#id_sancionado_ejecutar').val(id_sancionado);
		$.confirmarejec('Subir ejecución',{
			 aceptar: function(){
				 var ejecucion = new FormData(document.getElementById("form_ejecucion"));
				 $.ajax({
					 url: app.url + 'panel/subirEjecucion',
					 type: 'POST',
					 cache: false,
					 data: ejecucion,
					 processData: false,
					 contentType: false,
					 success: function(resultado){
						 if(resultado.exito){
							 $.noticia(resultado.mensaje,'success');
							 $.post(app.url + 'panel/historialFuncionario',{ id_funcionario : id_funcionario },function(resultado){
								 $.each(resultado.infoDetalle, function(index,value){
									 $('#nombre').html(value.nombre + ' ' + value.ap_paterno + ' ' + value.ap_materno);
									 if ( value.direccion ) {
										 $('#direccion').html(value.direccion + ' ' + value.colonia + ' ' + value.cp);
									 };
									 $('#telefonos').html(value.telefonos);
									 if ( value.correo ) {
										 $('#correo').html('<a href="mailto:' + value.correo + '" target="_blank">' + value.correo + '</a>');
									 };
									 $('#dep-ultima').html(resultado.dependenciaUltima);
									 $('#car-ultimo').html(resultado.cargoUltimo);
									 });
									 $('#infoSanciones').html(resultado.historialSanciones);
									 $('#btn-sancionar-historial').attr( 'href' , 'panel/sancionar/'+$(el).attr('data-id-funcionario') );
									 $mostrarHistorial();
							 },'json');
						 }
						 else {
							 if ( resultado.exito === false ) {
 											$.noticia(resultado.mensaje,'error');
 										};
						 }
					 }
				 });
			// 	$.post(app.url + 'panel/eliminar_sancion',{ id_sancionado : id_sancionado }, function(resultado){
			// 		if (resultado.exito === true){
			// 			$.noticia(resultado.mensaje,'success');
			// 			$.post(app.url + 'panel/historialFuncionario',{ id_funcionario : id_funcionario },function(resultado){
			// 				$.each(resultado.infoDetalle, function(index,value){
			// 					$('#nombre').html(value.nombre + ' ' + value.ap_paterno + ' ' + value.ap_materno);
			// 					if ( value.direccion ) {
			// 						$('#direccion').html(value.direccion + ' ' + value.colonia + ' ' + value.cp);
			// 					};
			// 					$('#telefonos').html(value.telefonos);
			// 					if ( value.correo ) {
			// 						$('#correo').html('<a href="mailto:' + value.correo + '" target="_blank">' + value.correo + '</a>');
			// 					};
			// 					$('#dep-ultima').html(resultado.dependenciaUltima);
			// 					$('#car-ultimo').html(resultado.cargoUltimo);
			// 					});
			// 					$('#infoSanciones').html(resultado.historialSanciones);
			// 					$('#btn-sancionar-historial').attr( 'href' , 'panel/sancionar/'+$(el).attr('data-id-funcionario') );
			// 					$mostrarHistorial();
			// 			},'json');
			// 		}else{
			// 			if ( resultado.exito === false ) {
			// 				$.noticia(resultado.mensaje,'error');
			// 			};
			// 		};
			// 	},'json');
			}
		});
		});







	$('#btn-guardar-edicion').click(function(){
		$.post(app.url + 'panel/guardar_cambios_funcionario', $('#form-edicion').serialize(), function(resultado){
			if(resultado.exito === true){
				document.getElementById("form-edicion").reset();
				$('#form-edicion div .error').removeClass('error');
				$('#id_funcionario_edicion').val('');
				$('#modal-edicion').modal('hide');
				$('#buscar').click();
				$.noticia(resultado.mensaje,'success');
			}else{
				if ( resultado.exito === false ) {
					first = true;
					$.each(resultado.mensaje, function(campo, aviso){
						if (first){
							$('div .error').removeClass('error');
							error = new Object({ campo: campo, mensaje: aviso });
							first = false;
						}
						$('#'+campo).parents('.control-group').addClass('error');
					});
					$.noticia(error.mensaje, 'danger');
					$('#'+error.campo).focus();
				};
			};
		},'json');
		return false;
	});
    $(document).ready(function() {
    	var sancionado = '<?php echo $funcionario_sancionado; ?>';
		if (sancionado) {
			$.post(app.url + 'panel/historialFuncionario',{ id_funcionario : sancionado },function(resultado){
				$.each(resultado.infoDetalle, function(index,value){
					$('#nombre').html(value.nombre + ' ' + value.ap_paterno + ' ' + value.ap_materno);
					if ( value.direccion ) {
						$('#direccion').html(value.direccion + ' ' + value.colonia + ' ' + value.cp);
					};
					$('#telefonos').html(value.telefonos);
					if ( value.correo ) {
						$('#correo').html('<a href="mailto:' + value.correo + '" target="_blank">' + value.correo + '</a>');
					};
					$('#dep-ultima').html(resultado.dependenciaUltima);
					$('#car-ultimo').html(resultado.cargoUltimo);
	    		});
	    		$('#infoSanciones').html(resultado.historialSanciones);
	    		$('#btn-sancionar-historial').attr( 'href' , 'panel/sancionar/'+sancionado);
	    		$mostrarHistorial();
			},'json');
			$.noticia('Se ha sancionado al Funcionario','success');
		};
	});
});
</script>
<form id="buscador" name="buscador" class="form-search well" action="#" method="post" onsubmit="return(false)">
	<div class="input-append">
		<input type="text" class="input-xxlarge search-query conv-mayu" id="busqueda" name="busqueda" placeholder="Buscar Funcionario" autofocus="autofocus"/>
		<button type="submit" id="buscar" name="buscar" class="btn btn-success"><i class="icon-search icon-white"></i> Buscar</button>
	</div>
</form>
<form id="detalle-historial" action="#" method="post" class="hide form-horizontal">
	<a id="btn-sancionar-historial" href="" class="btn btn-danger opcion pull-right" title="Sancionar"><i class="icon-thumbs-down icon-white"></i></a>
	<span class="pull-right">&nbsp;</span>
	<button id="btn-esconder" type="button" class="btn btn-primary pull-right opcion" title="Ocultar Historial" style="margin-bottom:-30px;"><i class="icon-minus-sign icon-white"></i></button>
	<input type="hidden" id="id_funcionario" name="id_funcionario">
	<legend id="nombre"></legend>
	<div class="row-fluid">
		<div class="span6">
			<h4 id="dep-ultima"></h4>
			<h5 id="car-ultimo"></h5>
		</div>
		<div class="span6">
			<p id="direccion"></p>
			<p id="telefonos"></p>
			<span id="correo"></span>
		</div>
	</div>
	<br>
	<legend>Historial de Sanciones</legend>
	<div id="infoSanciones"></div>
</form>
<div id="vaciarResultados">
	<?=$listado?>
</div>
<div class="modal hide fade" id="modal-edicion" style="width:817px;left:40%">
	<div class="modal-header lead">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<span>Edición de Funcionario</span>
	</div>
	<div class="modal-body">
		<form id="form-edicion" name="form-edicion" action="#" method="POST" onsubmit="return(false)" >
			<input type="hidden" id="id_funcionario_edicion" name="id_funcionario_edicion" />
			<div class="row-fluid">
			    <div class="span6">
			    	<div class="control-group">
						<label class="control-label">Nombre</label>
						<div class="controls">
							<input type="text" class="span12 conv-mayu" id="nombre_edicion" name="nombre_edicion">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Apellido Paterno</label>
						<div class="controls">
							<input type="text" class="span12 conv-mayu" id="pat_edicion" name="pat_edicion" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Apellido Materno</label>
						<div class="controls">
							<input type="text" class="span12 conv-mayu" id="mat_edicion" name="mat_edicion" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Correo Eléctronico</label>
						<div class="controls">
							<input type="text" class="span12" id="correo_edicion" name="correo_edicion" >
						</div>
					</div>
			    </div>
			    <div class="span6">
			    	<div class="control-group">
						<label class="control-label">Dirección</label>
						<div class="controls">
							<textarea class="span12 conv-mayu" id="direccion_edicion" name="direccion_edicion" rows="3" ></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Colonia</label>
						<div class="controls">
							<input type="text" class="span12 conv-mayu" id="col_edicion" name="col_edicion" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Código Postal</label>
						<div class="controls">
							<input type="text" class="span12 conv-mayu" id="cp_edicion" name="cp_edicion">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Teléfonos</label>
						<div class="controls">
							<textarea class="span12 conv-mayu" id="tel_edicion" name="tel_edicion" rows="3" ></textarea>
						</div>
					</div>
			    </div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<button type="reset" class="btn" data-dismiss="modal" id="btn-cancelar-edicion">Cancelar</button>
		<button type="submit" class="btn btn-primary" id="btn-guardar-edicion">Guardar cambios</button>
	</div>
</div>
